import { firestore } from 'firebase';
import { Time } from '@angular/common';
import { DocumentReference } from '@angular/fire/firestore';

export interface Event {
    name: string;
    location: firestore.GeoPoint;
    time: {
        startTime: Time;
        endTime?: Time;
    };
    date: {
        startDate: Date;
        endDate?: Date;
    };
    SalahRef?: DocumentReference;
}
