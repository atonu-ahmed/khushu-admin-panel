import { DocumentReference } from '@angular/fire/firestore';
import { Salah } from './salah.model';

export interface User {
    uid: string;
    roles: {
        imam?: boolean;
        admin?: boolean;
    };
    name?: {
        firstName?: string;
        lastName?: string;
    };
    hashPin?: string;
    email?: string;
    emailVerified?: boolean;
    address?: string;
    zipCode?: string;
    country?: string;
    city?: string;
    state?: string;
    photoURL?: string;
    imamPhotoURL?: string;
    phone?: string;
    phoneVerified?: boolean;
    masjidRef?: DocumentReference;
    mosqueSalahRefCollection?: DocumentReference[];
    mySalah?: Salah[];
    eventsRefCollection?: DocumentReference[];
}

