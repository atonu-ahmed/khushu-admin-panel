export interface Country {
    name: string;
    extension: number;
    language: string;
    iso?: string;
}
