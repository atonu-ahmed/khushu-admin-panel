import { firestore } from 'firebase';
import { DocumentReference } from '@angular/fire/firestore';

export interface Masjid {
    name: string;
    status: boolean;
    verified: boolean;
    location: firestore.GeoPoint;
    email?: string;
    zipCode?: string;
    country?: string;
    city?: string;
    state?: string;
    address?: string;
    photoURL?: string;
    imamRef?: DocumentReference;
    userRef?: DocumentReference;
    suggestedBy?: DocumentReference;

}
