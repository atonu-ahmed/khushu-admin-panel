import { DocumentReference } from '@angular/fire/firestore';
import { Timestamp } from '@firebase/firestore-types';

export interface Message {
  name: string;
  body: string;
  inquiryType: string;
  phone: string;
  readByAdmin: boolean;
  timestamp: Timestamp;
  email?: string;
}
