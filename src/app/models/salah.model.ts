import { DocumentReference } from '@angular/fire/firestore';
import { firestore } from 'firebase';
import { Time } from '@angular/common';
import { Timestamp } from '@firebase/firestore-types';


export interface Salah {
    name: string;
    wakt?: string;
    time: {
        startTime: {hours: string, minutes: string};
        endTime?: {hours: string, minutes: string};
    };
    date: {
        startDate?: Timestamp;
        endDate?: Timestamp;
    };
    masjidRef?: DocumentReference;
    location?: firestore.GeoPoint;
    disabled?: boolean;
    repeat?: {
        sunday?: boolean;
        monday?: boolean;
        tuesday?: boolean;
        wednesday?: boolean;
        thursday?: boolean;
        friday?: boolean;
        saturday?: boolean;
    };
}
