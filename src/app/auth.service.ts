import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument,
  DocumentReference,
} from '@angular/fire/firestore';
import { Observable, of, Subscriber, Subscription } from 'rxjs';
import { switchMap, tap, startWith, map } from 'rxjs/operators';
import { auth } from 'firebase';
import * as firebase from 'firebase';
import { Router } from '@angular/router';
import { WindowService } from './window.service';
import { User } from './models/user.model';

export class PhoneNumber {
  // country: string;
  // area: string;
  // prefix: string;
  // line: string;

  // get e164() {
  //   const num = this.country + this.area + this.prefix + this.line;
  //   return `${num}`;
  // }
  number: string;

}
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user$: Observable<User>;
  token: string;
  accountchecker: Subscription;

  windowRef: any;
  phoneNumber = new PhoneNumber();
  verificationCode: string;
  user: any;

  constructor(
    public win: WindowService,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router
  ) {
    (this.user$ = this.afAuth.authState.pipe(
      switchMap((user: firebase.User) => {
        if (user) {
          this.afs
            .doc<User>(`users/${user.uid}`)
            .valueChanges()
            .subscribe(userdata => console.log(userdata));
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    )),
      // Add these lines to set/read the user data to local storage
      tap(user => localStorage.setItem('user', JSON.stringify(user))),
      startWith(JSON.parse(localStorage.getItem('user')));
  }

  // async googleSignin() {
  //   const provider = new auth.GoogleAuthProvider();
  //   const credential = await this.afAuth.auth.signInWithPopup(provider);
  //   return this.updateUserData(credential.user, 'google');
  // }

  phoneWindowRef() {
    this.windowRef = this.win.windowRef;
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    this.windowRef.recaptchaVerifier.render();
  }

  sendLoginCode() {
    const appVerifier = this.windowRef.recaptchaVerifier;
    const num = this.phoneNumber.number;
    this.afAuth.auth.signInWithPhoneNumber(num, appVerifier)
      .then(result => {
        this.windowRef.confirmationResult = result;
      })
      .catch(error => console.log(error));
  }

  verifyLoginCode(user: any) {
    this.windowRef.confirmationResult
      .confirm(this.verificationCode)
      .then((result) => {
        console.log(result.user.uid);
        user.uid = result.user.uid;
        this.addUser(user);
      })
      .catch(error => console.log(error, 'Incorrect Code Entered'));
  }
  addUser(newUser: User) {

    this.afs.collection('users')
    .doc(newUser.uid).set(newUser)
      .then(() => {
        console.log('Document id set as', newUser.uid);
      })
      .catch(error => {
        console.log('Error Adding Document', error);
      });

  }

  phoneAuth(num, appVerifier) {
    this.afAuth.auth.signInWithPhoneNumber(num, appVerifier);
  }

  private updateUserData(user: firebase.User, provider: string) {
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(
      `users/${user.uid}`
    );

    let data: User = null;

    switch (provider) {
      case 'google':
        data = {
          uid: user.uid,
          email: user.email,
          photoURL: user.photoURL,
          roles: {
            imam: false,
            admin: false
          }
        };
        break;
      case 'password':
        data = {
          uid: user.uid,
          email: user.email,
          roles: {
            imam: false,
            admin: false
          }
        };
        break;
    }
    // return this.afs.collection('users').add(data);
    return userRef.set(data, { merge: true });
  }

  emailSignUp(email: string, password: string) {
    this.afAuth.auth
      .createUserWithEmailAndPassword(email, password)
      .catch(error => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        if (errorCode === 'auth/email-already-in-use') {
          this.emailSignIn(email, password);
        }
        if (errorCode === 'auth/weak-password') {
          alert('The password is too weak.');
        } else {
          alert(errorMessage);
        }
        console.log(error);
      })
      .then((response: any) => {
        this.updateUserData(response.user, 'password');
        this.router.navigate(['/']);
        this.emailSignIn(email, password);
        console.log('User Signed in Automatically');
      });
  }

  emailSignIn(email: string, password: string) {
    this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        const userEmail = email;
        const uId = this.afAuth.auth.currentUser.uid;
        console.log(uId);
        const docRef = this.afs.collection('users', ref => ref.where('uid', '==', uId));

        this.accountchecker = docRef.snapshotChanges()
          .pipe(map(action =>
            action.map(a => {

              const doc = a.payload.doc.data() as User;

              return doc;

            }))).subscribe((doc) => {
              if (doc.length === 0) {
                this.afs.collection('users').doc(uId).set({
                  uid: uId,
                  email: userEmail,
                  roles: {
                    admin: true,
                    imam: false
                  }

                }).then(() => {
                  this.router.navigate(['/']);
                  this.afAuth.auth.currentUser.getIdToken().then((token: string) => {
                    this.token = token;
                    console.log('User Signed in Email Password');
                  });

                });
              } else {
                this.router.navigate(['/']);
                this.afAuth.auth.currentUser.getIdToken().then((token: string) => {
                  this.token = token;
                  console.log('User Signed in Email Password');
                });
              }



            });



      })
      .catch(error => console.log(error));

  }

  async signOut() {
    await this.afAuth.auth.signOut();
    this.token = null;
    console.log('successfully signed out', this.currentUser());
    this.router.navigate(['/auth/login']);
  }

  getToken() {
    if (this.afAuth.auth.currentUser) {
      this.afAuth.auth.currentUser
        .getIdToken()
        .then((token: string) => (this.token = token));
      return this.token;
    }
  }

  isAuthenticated() {
    return this.token != null;
  }

  currentUser() {
    return this.afAuth.auth.currentUser;
  }

  accountcheckerDestroyer() {
    this.accountchecker.unsubscribe();
  }

  // authStateCheck() {
  //   this.afAuth.user
  //     .subscribe(
  //       (user: User) => {
  //         if (user != null) {
  //           this.getToken();
  //           console.log(this.currentUser());
  //         }
  //       });
  // }

  // authStateChanged() {
  //   this.afAuth.auth.onAuthStateChanged((user) => {
  //     console.log(user, 'changed');
  //   });
  // }

  // authTokenChange() {
  //   this.afAuth.auth.onIdTokenChanged((user) => {
  //     if (user) {
  //       console.log(user, 'tokenChanged');
  //     }
  //   });
  // }

  // setAuthPersistance() {
  //   this.afAuth.auth.setPersistence('local');
  // }
}
