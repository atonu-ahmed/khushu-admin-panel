import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { AdminDashboardComponent } from './admin-dashboard/dashboard.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { AuthGuard } from '../auth.guard';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
  //   {
  //   path: 'dashboard',
  //   component: ECommerceComponent,
  // },
  //  {
  //   path: 'iot-dashboard',
  //   component: DashboardComponent,
  // },
  {
    path: 'admin-dashboard',
    component: AdminDashboardComponent,
  },
  {
    path: 'users',
    loadChildren: './users/users.module#UsersModule',
  },
  {
    path: 'masjids',
    loadChildren: './masjids/masjids.module#MasjidsModule',
  },
  {
    path: 'salah',
    loadChildren: './salah/salah.module#SalahModule',
  },
  {
    path: 'events',
    loadChildren: './events/events.module#EventsModule',
  },
  {
    path: 'email',
    loadChildren: './email/email.module#EmailModule',
  },

  //  {
  //   path: 'ui-features',
  //   loadChildren: './ui-features/ui-features.module#UiFeaturesModule',
  // }, {
  //   path: 'modal-overlays',
  //   loadChildren: './modal-overlays/modal-overlays.module#ModalOverlaysModule',
  // }, {
  //   path: 'extra-components',
  //   loadChildren: './extra-components/extra-components.module#ExtraComponentsModule',
  // }, {
  //   path: 'bootstrap',
  //   loadChildren: './bootstrap/bootstrap.module#BootstrapModule',
  // }, {
  //   path: 'maps',
  //   loadChildren: './maps/maps.module#MapsModule',
  // }, {
  //   path: 'charts',
  //   loadChildren: './charts/charts.module#ChartsModule',
  // }, {
  //   path: 'editors',
  //   loadChildren: './editors/editors.module#EditorsModule',
  // }, {
  //   path: 'forms',
  //   loadChildren: './forms/forms.module#FormsModule',
  // }, {
  //   path: 'tables',
  //   loadChildren: './tables/tables.module#TablesModule',
  // }, {
  //   path: 'miscellaneous',
  //   loadChildren: './miscellaneous/miscellaneous.module#MiscellaneousModule',
  // },

  {
    path: '',
    redirectTo: 'admin-dashboard',
    pathMatch: 'full',
  }, {
    path: '**',
    component: NotFoundComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
