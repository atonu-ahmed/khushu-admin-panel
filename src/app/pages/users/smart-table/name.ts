import { Component, Input, OnInit } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';
import { UserService } from '../user.service';

@Component({
  template: `
  {{renderFirstName}} {{ renderLastName}}
  `,
  styleUrls: ['./button.css'],
})
export class Name implements ViewCell, OnInit {


  renderFirstName: string;
  renderLastName: string;

  constructor(
    private usersevice: UserService,
  ) {}

  @Input() value: any;
  @Input() rowData: any;
  ngOnInit() {
    // this.renderFirstName = this.value.firstName.toString().toUpperCase();
    // this.renderLastName = this.value.lastName.toString().toUpperCase();
    this.renderFirstName = this.value.firstName;
    this.renderLastName = this.value.lastName;
  }
}
