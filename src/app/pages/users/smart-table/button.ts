import { Component, Input, OnInit } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';
import { UserService } from '../user.service';

@Component({
  templateUrl: './button.html',
  styleUrls: ['./button.css'],
})
export class TFbutton implements ViewCell, OnInit {


  renderValue: string;

  constructor(
    private usersevice: UserService,
  ) {}

  @Input() value: any;
  @Input() rowData: any;
  ngOnInit() {
    // console.log(this.rowData);
    // this.renderValue = this.value.toString().toUpperCase();
  }
  updateImam(
    userId: string,
    statusAdmin: boolean,
    statusImam: boolean,
  ) {
    // Changing Imams state
    statusImam = this.usersevice.checkstatus(statusImam);

    this.usersevice.editAdmin(userId, statusAdmin, statusImam);
  }

}
