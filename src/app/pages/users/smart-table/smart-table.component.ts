import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  DocumentReference,
} from '@angular/fire/firestore';
import { SmartTableData } from '../../../@core/data/smart-table';
import { UserService } from '../user.service';
import { Observable, Subscription } from 'rxjs';
import { User } from '../../../models/user.model';
import { TFbutton } from './button';
import { ProfilePicComponent } from '../../../profile-pic/profile-pic.component';


export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
@Component({
  selector: 'ngx-smart-table',
  templateUrl: './smart-table.component.html',
  styles: ['./smart-table.component.scss'],
})
export class SmartTableComponent implements OnInit {
  arrayUser: Omit<User, 'uid' | 'roles'> = {};

  settings = {
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: true,
      custom: [],
      position: 'right',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      photoURL: {
        title: 'Profile Picture',
        type: 'custom',
        renderComponent: ProfilePicComponent,
        filter: false,
      },
      name: {
        title: 'Name',
        type: 'string',
      },
      email: {
        title: 'Email',
        type: 'string',
      },
      phone: {
        title: 'Phone',
        type: 'string',
      },
      country: {
        title: 'Country',
        type: 'string',
      },
      address: {
        title: 'Address',
        type: 'string',
      },
      zipCode: {
        title: 'Zip Code',
        type: 'string',
      },
      city: {
        title: 'City',
        type: 'string',
      },
      state: {
        title: 'State',
        type: 'string',
      },
      roles: {
        title: 'Role',
        type: 'custom',
        renderComponent: TFbutton,
        filter: false,
      },
    },
  };

  adminTableSettings = {
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: true,
      custom: [],
      position: 'right',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      photoURL: {
        title: 'Profile Picture',
        type: 'custom',
        filter: false,
        renderComponent: ProfilePicComponent,
      },
      name: {
        title: 'Name',
        type: 'string',
      },
      email: {
        title: 'Email',
        type: 'string',
      },
      phone: {
        title: 'Phone',
        type: 'string',
      },
      country: {
        title: 'Country',
        type: 'string',
      },
      address: {
        title: 'Address',
        type: 'string',
      },
      zipCode: {
        title: 'Zip Code',
        type: 'string',
      },
      city: {
        title: 'City',
        type: 'string',
      },
      state: {
        title: 'State',
        type: 'string',
      },
    },
  };
  // source: Observable<{}[]>;
  newUsersArray: any = [];
  adminsArray: any = [];
  source: LocalDataSource;
  adminTableSource: LocalDataSource;
  usersObservable$: Subscription;
  usersObservableOld$: Observable<{ data: User; id: string }[]>;
  modalView = false;
  editData: any;
  // userEdit: Omit<User, 'uid' | 'roles'> = {
  // };
  userEdit: any;

  constructor(
    private service: SmartTableData,
    // // auth is not used
    private afs: AngularFirestore,
    // // private auth: AuthService,
    private userService: UserService,
  ) { }
  ngOnInit(): void {
    this.usersObservable$ = this.userService
      .getUser()
      .subscribe((value: { data: User; id: string }[]) => {
        this.newUsersArray = [];
        this.adminsArray = [];
        const name = {
          name: '',
        };
        value.forEach(newDataset => {
          if (newDataset.data.name) {
            name.name =
              newDataset.data.name.firstName +
              ' ' +
              newDataset.data.name.lastName;
          }
          this.newUsersArray.push(Object.assign(newDataset.data, name));
          if (newDataset.data.roles.admin) {
            this.adminsArray.push(Object.assign(newDataset.data, name));
          }

        });
        this.source = new LocalDataSource(this.newUsersArray);
        this.adminTableSource = new LocalDataSource(this.adminsArray);

      });
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.deleteUser(event.data.uid);
    } else {
      event.confirm.reject();
    }
  }

  deleteUser(userId: string) {
    this.userService.deleteUser(userId);
  }

  modalToggle() {
    this.modalView = !this.modalView;
  }
  editUser(event) {
    const fullName = {
      name: {
        firstName: '',
        lastName: '',
      },
    };
    const data = {
      email: '',
      phone: '',
      country: '',
      address: '',
      id: '',
      photoURL: '',
      city: '',
      state: '',
      zipCode: '',
    };
    const newFullName = event.data.name.split(' ', 2);
    fullName.name.firstName = newFullName[0];
    fullName.name.lastName = newFullName[1];
    if (!fullName.name.lastName) {
      fullName.name.lastName = '';
    }
    if (event.data.email) {
      data.email = event.data.email;
    }
    if (event.data.phone) {
      data.phone = event.data.phone;
    }
    if (event.data.country) {
      data.country = event.data.country;
    }
    if (event.data.address) {
      data.address = event.data.address;
    }
    if (event.data.photoURL) {
      data.photoURL = event.data.photoURL;
    }
    if (event.data.city) {
      data.city = event.data.city;
    }
    if (event.data.state) {
      data.state = event.data.state;
    }
    if (event.data.zipCode) {
      data.zipCode = event.data.zipCode;
    }

    data.id = event.data.uid;

    this.userEdit = Object.assign(data, fullName);

    this.modalToggle();

  }

  saveEditedUser() {
    // tslint:disable-next-line: no-console
    this.userService.editUser(this.userEdit, this.userEdit.id);
    this.modalToggle();
  }
  updateAdmin(userId: string, statusAdmin: boolean, statusImam: boolean) {
    // Changing admins state
    statusAdmin = this.userService.checkstatus(statusAdmin);

    this.userService.editAdmin(userId, statusAdmin, statusImam);
  }

  updateImam(
    // userId: string,
    // statusAdmin: boolean,
    // statusImam: boolean,
    event: any,
  ) {
    // Changing Imams state
    // statusImam = this.userService.checkstatus(statusImam);
    // this.userService.editAdmin(userId, statusAdmin, statusImam);
  }
}
