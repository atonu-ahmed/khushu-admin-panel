import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { TablesRoutingModule,  } from './users-routing.module';
import { UsersComponent } from './users.component';
import { SmartTableComponent } from './smart-table/smart-table.component';
import { AddUserComponent } from './add-user/add-user.component';
import { MasjidAdminTableComponent } from './masjid-admin/masjid-admin-table.component';
import { CustomRenderComponent } from './masjid-admin/button';
import { TFbutton } from './smart-table/button';
import { Name } from './smart-table/name';
import { AuthModule } from '../auth/auth.module';
import { ProfilePicModule } from '../../profile-pic/profile-pic.module';


@NgModule({
  imports: [
    ThemeModule,
    TablesRoutingModule,
    Ng2SmartTableModule,
    AuthModule,
    ProfilePicModule,


  ],
  entryComponents : [
    CustomRenderComponent,
    TFbutton,
    Name,
  ],
  declarations: [
    SmartTableComponent,
    UsersComponent,
    AddUserComponent,
    MasjidAdminTableComponent,
    CustomRenderComponent,
    TFbutton,
    Name,
  ],
})
export class UsersModule {
}
