import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from './users.component';
import { SmartTableComponent } from './smart-table/smart-table.component';
import { AddUserComponent } from './add-user/add-user.component';
import { SignUpComponent } from '../auth/sign-up.component';
import { MasjidAdminTableComponent } from './masjid-admin/masjid-admin-table.component';

const routes: Routes = [{
  path: '',
  component: UsersComponent,
  children: [{
    path: 'view-users',
    component: SmartTableComponent,
  },
  {
    path: 'add-user',
    component: SignUpComponent,
  },
  {
    path: 'masjid-admins',
    component: MasjidAdminTableComponent,
  },
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TablesRoutingModule { }

export const routedComponents = [
  UsersComponent,
  SmartTableComponent,
];
