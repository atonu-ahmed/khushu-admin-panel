import { Injectable } from '@angular/core';
import {
  AngularFirestoreCollection,
  AngularFirestore,
  DocumentReference,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { User } from '../../models/user.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Masjid } from '../../models/masjid.model';
import { resolveSoa } from 'dns';
import { firestore } from 'firebase';

interface ImamReq {
  userRef: DocumentReference;
  masjidRef: DocumentReference;
}
@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private afs: AngularFirestore) { }
  userCollection: AngularFirestoreCollection<User> = this.afs.collection(
    'users',
  );

  userObservable$: Observable<{ data: User; id: string }[]>;
  masjidObservable$: Observable<{ data: Masjid; id: string }[]>;
  imamCollection: AngularFirestoreCollection<User>
  = this.afs.collection('users', ref => ref.where('roles.imam', '==', true));

  addUser(newUser: User): void {
// tslint:disable-next-line: no-console
    console.log('Here at userervice');
    this.userCollection
      .add(newUser)
      .then((docRef: DocumentReference) => {
// tslint:disable-next-line: no-console
        console.log('Document id set as', docRef.id);
      })
      .catch(error => {
// tslint:disable-next-line: no-console
        console.log('Error Adding Document', error);
      });
  }

  updateUserbyId(id: string, arr: any) {
    this.afs
      .doc(`users/${id}`)
      .valueChanges()
      .subscribe((userData: User) => {
        // Inserting data by looping the arr obj
        // tslint:disable-next-line: forin
        for (const key in arr) {
          // tslint:disable-next-line: prefer-const
          let value = arr[key];
          userData[key] = value;
        }
        this.afs.doc(`users/${id}`).set(userData);
      });
  }

  getUser() {
    this.userObservable$ = this.userCollection.snapshotChanges().pipe(
      map(action =>
        action.map(a => {
          const data = a.payload.doc.data() as User;
          const id = a.payload.doc.id;
          return { data, id };
        }),
      ),
    );

    return this.userObservable$;
  }

  deleteUser(userId: string) {
    const document = this.userCollection.doc(`${userId}`);
    document
      .delete()
      .then(() => {
// tslint:disable-next-line: no-console
        console.log(`Document of id ${userId} deleted Successfully`);
      })
      .catch(error => {
// tslint:disable-next-line: no-console
        console.log('Error Removing Document', error);
      });
  }

  editUser(userData: any, userId: string) {
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(
      `users/${userId}`
    );

    userRef
      .update(userData)
      .then(() => {
        console.log(`successfully edited User`);
      })
      .catch(error => {
        console.error(error);
      });
  }

  editAdmin(userId: string, statusAdmin: boolean, statusImam: boolean) {
    this.userCollection
      .doc(userId)
      .update({
        roles: {
          admin: statusAdmin,
          imam: statusImam,
        },
      })
      // tslint:disable-next-line: only-arrow-functions
      .then(function() {
// tslint:disable-next-line: no-console
        console.log('Document successfully updated!');
      })
      // tslint:disable-next-line: only-arrow-functions
      .catch(function(error) {
// tslint:disable-next-line: no-console
        console.log('Error found data not changed' + error);
      });
  }


  getImams() {
    return this.imamCollection.snapshotChanges()
      .pipe(
        map(action =>
          action.map(a => {
            const data = a.payload.doc.data() as User;
            const id = a.payload.doc.id;
            let masjidId: string;
            let masjid: Masjid;
            const mosquePromise = new Promise((resolve, reject) => {
              if (data.masjidRef) {
                masjidId = data.masjidRef.id;
                this.afs.doc(`masajid/${masjidId}`).ref.get().then(
                  (doc) => {
                    masjid = doc.data() as Masjid;
                    resolve({ data, id, masjid, masjidId });
                  }
                );
              } else {
                reject({ data, id });
              }
            }).then(
              (dataWithMasjid: { data: User, id: string, Masjid: Masjid, MasjidId: string }) => dataWithMasjid
            )
              .catch(
                (dataNoMasjid: { data: User, id: string }) => dataNoMasjid
              );
            return mosquePromise;
          })
        )
      );
  }
  addImam(userId: string, masjidId: string, requestId: string) {
    const userReference = this.afs.doc('users/' + userId).ref;
    const masjidReference = this.afs.doc('masajid/' + masjidId).ref;

    this.afs.collection('masajid').doc(masjidId).set({
      imamRef: userReference
    }, { merge: true })
    .then(() => { console.log('Document Updated'); })
    .catch((error) => { console.log(error); });

    this.userCollection.doc(userId).set({
      masjidRef: masjidReference,
      'roles.imam' : true
    }, {merge: true})
    .then(() => { console.log('Document Updated'); })
    .catch((error) => { console.log(error); });

    this.userCollection.doc(userId).update({
      'roles.imam' : true
    })
    .then(() => { console.log('Document Updated'); })
    .catch((error) => { console.log(error); });

    this.afs.collection('imamRequests').doc(requestId).delete()
    .then(() => console.log('Document Updated') )
    .catch((error) => console.log(error));

  }

  getImamReq() {
    const imamReq = this.afs.collection('imamRequests').snapshotChanges()
      .pipe(
        map(action =>
          action.map(a => {
            const data = a.payload.doc.data() as ImamReq;
            const id = a.payload.doc.id;
            const imamID = data.userRef.id;
            const masjidID = data.masjidRef.id;
            const imamPromise: Promise<User> = new Promise((resolve, reject) => {
              this.afs.doc(`users/${imamID}`).ref.get()
                .then((imam) => {
                  resolve(imam.data() as User);
                })
                .catch((error) => reject(error));
            });
            const masjidPromise: Promise<Masjid> = new Promise((resolve, reject) => {
              this.afs.doc(`masajid/${masjidID}`).ref.get()
                .then((masjid) => {
                  resolve(masjid.data() as Masjid);
                })
                .catch((error) => reject(error));
            });

            return ({ id, imamPromise, masjidPromise, masjidID, imamID });
          })
        )
      );
    return imamReq;
  }

  // getImamReq() {
  //   const imamReq = this.afs.collection('imamRequests').snapshotChanges().pipe(
  //     map(action =>
  //       action.map(a => {
  //         const data = a.payload.doc.data() as ImamReq;
  //         const id = a.payload.doc.id;
  //         return { data, id };
  //       }),
  //     ),
  //   );
  //   return imamReq;
  // }

  getImamReqOld() {
    const imamReq = this.afs.collection('imamRequest').snapshotChanges()
      .pipe(
        map(action =>
          action.map(a => {
            const data = a.payload.doc.data() as ImamReq;
            const id = a.payload.doc.id;
            const imamID = data.userRef.id;
            const masjidID = data.masjidRef.id;
            const imamPromise: Promise<User> = new Promise((resolve, reject) => {
              this.afs.doc(`users/${imamID}`).ref.get()
                .then((imam) => {
                  resolve(imam.data() as User);
                })
                .catch((error) => reject(error));
            });
            const masjidPromise: Promise<Masjid> = new Promise((resolve, reject) => {
              this.afs.doc(`masajid/${masjidID}`).ref.get()
                .then((masjid) => {
                  resolve(masjid.data() as Masjid);
                })
                .catch((error) => reject(error));
            });

            return ({ id, imamPromise, masjidPromise });
          })
        )
      );
    return imamReq;
  }

  editImam(userId: string, statusAdmin: boolean, statusImam: boolean) {
    this.userCollection
      .doc(userId)
      .update({
        roles: {
          admin: statusAdmin,
          imam: statusImam,
        },
      })
      // tslint:disable-next-line: only-arrow-functions
      .then(function() {
// tslint:disable-next-line: no-console
        console.log('Document successfully updated!');
      })
      // tslint:disable-next-line: only-arrow-functions
      .catch(function(error) {
// tslint:disable-next-line: no-console
        console.log('Error found data not changed' + error);
      });
  }
  checkstatus(status: boolean = true) {
    return !status;
  }
  deleteImamReq(reqId: string) {
    this.afs.collection('imamRequests').doc(reqId)
     .delete()
     .then(() => {
       console.log(`Document of id ${reqId} deleted Successfully`);
     })
     .catch(error => {
       console.log('Error Removing Document', error);
     });
 }
 removeImam(userId: string, masjidId: string) {

  this.afs.collection('masajid').doc(masjidId).update({
    userRef: firestore.FieldValue.delete()
  });
  this.userCollection.doc(userId).update({
    masjidRef: firestore.FieldValue.delete(),
    roles: {
      admin: false,
      imam: false

    }
  });


  }

  getEditUser(userId: string) {
    return this.afs
      .doc(`users/${userId}`).valueChanges();
  }

  getUserInfo(userId: string) {
    return this.afs
      .doc(`users/${userId}`).snapshotChanges()
      .pipe(
        map(action => {
          const userData = action.payload.data() as User;
          return userData;
          }));
  }
}
