import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableData } from '../../../@core/data/smart-table';
import { CustomRenderComponent } from './button';
import {
  AngularFirestoreCollection,
  AngularFirestore,
  DocumentReference,
} from '@angular/fire/firestore';
import { User } from '../../../models/user.model';
import { Observable, Subscription } from 'rxjs';
import { Masjid } from '../../../models/masjid.model';
import { UserService } from '../user.service';
import { NgForm } from '@angular/forms';
import { Name } from '../smart-table/name';
import { TFbutton } from '../smart-table/button';
import { MasajidService } from '../../masjids/masajid.service';
import { ProfilePicComponent } from '../../../profile-pic/profile-pic.component';

interface ImamReq {
  userRef: DocumentReference;
  masjidRef: DocumentReference;
}

@Component({
  selector: 'ngx-masjid-admin-table',
  templateUrl: './masjid-admin-table.component.html',
  styles: ['./masjid-admin-table.component.scss'],
})
export class MasjidAdminTableComponent implements OnInit {
  settings = {
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: true,
      custom: [],
      position: 'right'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      imamPhotoURL: {
        title: 'Profile Picture',
        type: 'custom',
        filter: false,
        renderComponent: ProfilePicComponent,
      },
      name: {
        title: 'Name',
        type: 'string'
      },
      masjidName: {
        title: 'Masjid Name',
        type: 'string'
      },
      email: {
        title: 'Email',
        type: 'string'
      },
      phone: {
        title: 'Phone',
        type: 'string'
      },
      country: {
        title: 'Country',
        type: 'string'
      },
      address: {
        title: 'Address',
        type: 'string'
      },
      zipCode: {
        title: 'Zip Code',
        type: 'string',
      },
      city: {
        title: 'City',
        type: 'string',
      },
      state: {
        title: 'State',
        type: 'string',
      },
      // roles: {
      //   title: 'Role',
      //   type: 'custom',
      //   renderComponent: TFbutton,
      // },
    },
  };
  adminApproveTableSettings = {
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: true,
      custom: [],
      position: 'right'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      photoURL: {
        title: 'Profile Picture',
        type: 'custom',
        filter: false,
        renderComponent: ProfilePicComponent,
      },
      name: {
        title: 'Name',
        type: 'string'
      },
      masjidName: {
        title: 'Masjid Name',
        type: 'string'
      },
      email: {
        title: 'Email',
        type: 'string'
      },
      phone: {
        title: 'Phone',
        type: 'string'
      },
      userAddress: {
        title: 'User Address',
        type: 'string',
      },
      masjidAddress: {
        title: 'Masjid Address',
        type: 'string',
      },
      verification: {
        title: 'Verification',
        type: 'custom',
        renderComponent: CustomRenderComponent,
        filter: false,
      },
    },
  };
  newUsersArray: any = [];
  allUsers: any = [];
  imamsWithMasjid: any[];
  imamsForMasjid: any[];
  imamsWithMasjidRequests: any[];

  imamsObservable: Subscription;
  usersObservable$: Subscription;
  masjidObservable: Subscription;

  imamReqSubscription: Subscription;
  imamReqCollection: { id: string; newImam: User; newMasjid: Masjid }[] = [];

  source: LocalDataSource;
  adminApprovalSource: LocalDataSource;
  usersObservable: Observable<{ data: User; id: string }[]>;

  constructor(
    private service: SmartTableData,
    private afs: AngularFirestore,
    private userService: UserService,
    private masjidSevice: MasajidService,
  ) { }
  ngOnInit(): void {

    this.imamsObservable = this.userService.getImams()
      .subscribe(
        (value: Promise<{ data: User, id: string, masjid?: Masjid, masjidId?: string }>[]) => {
          this.imamsForMasjid = [];
          const datas = {
            name: '',
            masjidName: '',
            id: '',
            masjidId: '',
          };
          value.forEach((dataset) => {
            dataset.then(
              (dataWithMasjid) => {
                datas.id = dataWithMasjid.id;
                datas.masjidId = dataWithMasjid.masjidId;
                if (dataWithMasjid.data.name) {

                  if (dataWithMasjid.data.name.lastName) {
                    datas.name = dataWithMasjid.data.name.firstName + ' ' + dataWithMasjid.data.name.lastName;
                  } else {
                    datas.name = dataWithMasjid.data.name.firstName;
                  }
                }

                if (dataWithMasjid.masjid) {
                  datas.masjidName = dataWithMasjid.masjid.name;
                }


                this.imamsForMasjid.push(Object.assign(dataWithMasjid.data, datas));
              },
            ).then(
              () => {
                console.log(this.imamsForMasjid);
                this.source = new LocalDataSource(this.imamsForMasjid);
              },
            )
              .catch(
                (dataNoMasjid) => {
                  console.log(dataNoMasjid)
                  if (dataNoMasjid.data) {
                  if (dataNoMasjid.data.name) {
                    if (dataNoMasjid.data.name.lastName) {
                      datas.name = dataNoMasjid.data.name.firstName + ' ' + dataNoMasjid.data.name.lastName;
                    } else {
                      datas.name = dataNoMasjid.data.name.firstName;
                    }
                  }
                  this.imamsForMasjid.push(Object.assign(dataNoMasjid.data, datas));
                }
              }
              );
          });

        });


        this.imamReqSubscription = this.userService.getImamReq()
      .subscribe(
        (value: { id: string; imamPromise: Promise<User>;
          masjidPromise: Promise<Masjid>; masjidID: string;  imamID: string }[]) => {
          this.imamReqCollection = [];
          value.forEach(dataset => {
            let newImam: User;
            let newMasjid: Masjid;
            let id: string;
            let masjidId: string;
            let imamID: string;
            dataset.imamPromise
              .then((imam) => newImam = imam)
              .then(() => {
                dataset.masjidPromise.then(
                  (masjid) => {
                    newMasjid = masjid;
                    id = dataset.id;
                    masjidId = dataset.masjidID;
                    imamID = dataset.imamID;
                  }
                )
                  .then(() => {
                    const newDataset = { id, newImam, newMasjid, masjidId, imamID};
                    const datas = {
                      name: '',
                      masjidName: '',
                      email: '',
                      phone: '',
                      userAddress: '',
                      masjidAddress: '',
                      userID: '',
                      requestID: '',
                      masjidID: '',
                      photoURL: '',
                      roles: null,
                    };

                    datas.userID = imamID;
                    datas.requestID = id;
                    datas.masjidID = masjidId;


                    if (newDataset.newImam && newDataset.newImam.name) {
                      datas.name = newImam.name.firstName + ' ' + newImam.name.lastName;
                    }
                    if (newDataset.newMasjid && newDataset.newMasjid.name) {
                      datas.masjidName = newMasjid.name;
                    }
                    if (newDataset.newMasjid && newDataset.newMasjid.address) {
                      datas.masjidAddress = newMasjid.address;
                    }
                    if (newDataset.newImam && newDataset.newImam.email) {
                      datas.email = newImam.email;
                    }
                    if (newDataset.newImam && newDataset.newImam.phone) {
                      datas.phone = newImam.phone;
                    }
                    if (newDataset.newImam && newDataset.newImam.address) {
                      datas.userAddress = newImam.address;
                    }
                    if (newDataset.newImam && newDataset.newImam.photoURL) {
                      datas.photoURL = newImam.photoURL;
                    }
                    if (newDataset.newImam && newDataset.newImam.roles) {
                      datas.roles = newImam.roles;
                    }
                    this.imamReqCollection.push(Object.assign(datas));

                  }).then(
                    () => {
                      this.adminApprovalSource = new LocalDataSource(this.imamReqCollection);
                      console.log(this.imamReqCollection);
                    },
                  );
              });
          });
        },
      );


    // this.userService
    //   .getUser()
    //   .subscribe((value: { data: User; id: string }[]) => {
    //     this.newUsersArray = [];
    //     const names = {
    //       name: ''
    //     };
    //     value.forEach(dataset => {
    //       if (!dataset.data.roles.imam) {
    //         if (dataset.data.name) {
    //         }
    //         this.newUsersArray.push(Object.assign(dataset.data));
    //       }
    //     });

    //     this.masjidSevice
    //       .getMasjid()
    //       .subscribe((value: { id: string; data: Masjid }[]) => {
    //         this.imamsWithMasjid = [];
    //         value.forEach(element => {
    //           this.imamsWithMasjid.push({
    //             data: {
    //               name: element.data.name,
    //               id: element.id,
    //               address: element.data.address,
    //             }
    //           });
    //         });
    //         this.masjidObservable = this.userService
    //           .getImamReq()
    //           .subscribe((value: { data: any; id: string; }[]) => {
    //             this.imamsWithMasjidRequests = [];

    //             this.userService
    //               .getUser()
    //               .subscribe((allUsers: { data: User; id: string }[]) => {
    //                 this.newUsersArray = [];

    //                 value.forEach(element => {
    //                   const datas = {
    //                     name: '',
    //                     masjidName: '',
    //                     email: '',
    //                     photoURL: '',
    //                     phone: '',
    //                     userID: '',
    //                     masjidID: '',
    //                     isAdmin: false,
    //                     requestID: '',
    //                     userAddress: '',
    //                     masjidAddress: '',
    //                   };

    //                   datas.requestID = element.id;

    //                   allUsers.forEach(dataset => {

    //                     // this.newUsersArray.push(Object.assign(dataset.data));

    //                     if (dataset.data.uid === element.data.userRef.id) {
    //                       datas.name = dataset.data.name.firstName + ' ' + dataset.data.name.lastName;
    //                       datas.email = dataset.data.email;
    //                       datas.phone = dataset.data.phone;
    //                       datas.photoURL = dataset.data.photoURL;
    //                       datas.userID = dataset.data.uid,
    //                         datas.isAdmin = dataset.data.roles.admin;
    //                       datas.userAddress = dataset.data.address;
    //                     }
    //                   });

    //                   this.imamsWithMasjid.forEach(masjids => {
    //                     if (element.data.masjidRef.id === masjids.data.id) {
    //                       datas.masjidName = masjids.data.name;
    //                       datas.masjidID = masjids.data.id;
    //                       datas.masjidAddress = masjids.data.address;
    //                     }
    //                   });

    //                   this.imamsWithMasjidRequests.push(Object.assign(datas));

    //                 });
    //                 this.adminApprovalSource = new LocalDataSource(this.imamsWithMasjidRequests);
    //               });

    //           });
    //       });

    //   });
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to remove this admin?')) {
      event.confirm.resolve();

      this.userService.removeImam(event.data.uid, event.data.masjidId);
    } else {
      event.confirm.reject();
    }
  }


  onDeleteConfirmRequest(event): void {
    if (window.confirm('Are you sure you want to remove this admin?')) {
      event.confirm.resolve();
        this.userService.deleteImamReq(event.data.requestID);
    } else {
      event.confirm.reject();
    }
  }

  deleteUser(userId: string) {
    this.userService.deleteUser(userId);
  }

  editUser(event) {
    this.userService.editUser(event.newData, event.data.uid);
    // this.userService.updateUserbyId(userId, this.arr);
  }
  updateAdmin(userId: string, statusAdmin: boolean, statusImam: boolean) {
    // Changing admins state
    statusAdmin = this.userService.checkstatus(statusAdmin);

    this.userService.editAdmin(userId, statusAdmin, statusImam);
  }

  updateImam(userId: string, statusAdmin: boolean, statusImam: boolean) {
    // Changing Imams state
    statusImam = this.userService.checkstatus(statusImam);

    this.userService.editAdmin(userId, statusAdmin, statusImam);
  }

}
