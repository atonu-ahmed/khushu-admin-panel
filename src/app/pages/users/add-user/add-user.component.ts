import { Component } from '@angular/core';

@Component({
  selector: 'ngx-form-inputs',
  styleUrls: ['./add-user.component.scss'],
  templateUrl: './add-user.component.html',
})
export class AddUserComponent {

  starRate = 2;
  heartRate = 4;
  radioGroupValue = 'This is value 2';
}
