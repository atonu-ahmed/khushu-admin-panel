import { NgModule } from '@angular/core';
// import { ChartjsLineComponent } from './charts/chartjs/chartjs-line.component';
import { PagesComponent } from './pages.component';
// import { DashboardModule } from './dashboard/dashboard.module';
// import { ECommerceModule } from './e-commerce/e-commerce.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { AdminDashboardModule } from './admin-dashboard/dashboard.module';

const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    // DashboardModule,
    AdminDashboardModule,
    // ECommerceModule,
    MiscellaneousModule,
  ],
  declarations: [
    // ChartjsLineComponent,
    ...PAGES_COMPONENTS,
  ],
})
export class PagesModule {
}
