import { Component, Input, OnInit } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';
import { UserService } from '../users/user.service';

@Component({
  template:
    `<div style="width: 150px" class="d-flex justify-content-center">
    <button  (click)="update()" class="yes-button mx-1">CONFIRM</button>`
  ,
  styleUrls: ['./buttonUsers.css'],
})
export class ImamButton implements ViewCell, OnInit {

  renderValue: string;

  @Input() value: string | number;
  @Input() rowData: any;

  update() {
    console.log(this.rowData);
    this.userService.addImam(this.rowData.userID, this.rowData.masjidID, this.rowData.requestID);
  }
  constructor(
    private userService: UserService,
  ) {}
  ngOnInit() {
    this.renderValue = this.value.toString().toUpperCase();
  }

}
