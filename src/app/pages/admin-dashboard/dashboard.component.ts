import { Component, OnDestroy, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { UserService } from '../users/user.service';
import { SalahService } from '../salah/salah.service';
import { MasajidService } from '../masjids/masajid.service';
import { CustomRenderComponent } from './button';
import { Masjid } from '../../models/masjid.model';
import { User } from '../../models/user.model';
import { ImamButton } from './buttonUsers';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class AdminDashboardComponent implements OnInit, OnDestroy {

  pendingMasjidSettings = {
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: true,
      custom: [],
      position: 'right',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: 'Masjid Name',
        type: 'string',
      },
      address: {
        title: 'Address',
        type: 'string',
      },
      city: {
        title: 'City',
        type: 'string',
      },
      state: {
        title: 'State',
        type: 'string',
      },
      country: {
        title: 'Country',
        type: 'string',
      },
      zipCode: {
        title: 'Zip Code',
        type: 'string',
      },
      suggestedBy: {
        title: 'Suggested By',
        type: 'string',
      },
      confirm: {
        title: 'Confirm',
        type: 'custom',
        renderComponent: CustomRenderComponent,
        filter: false,
      },
    },
  };

  pendingImamSettings = {
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: true,
      custom: [],
      position: 'right'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // profilepicture: {
      //   title: 'Profile Picture',
      //   type: 'string',
      //   filter: false,
      // },
      name: {
        title: 'Name',
        type: 'string'
      },
      masjidName: {
        title: 'Masjid Name',
        type: 'string'
      },
      email: {
        title: 'Email',
        type: 'string'
      },
      phone: {
        title: 'Phone',
        type: 'string'
      },
      userAddress: {
        title: 'User Address',
        type: 'string',
      },
      masjidAddress: {
        title: 'Masjid Address',
        type: 'string',
      },
      verification: {
        title: 'Verification',
        type: 'custom',
        renderComponent: ImamButton,
        filter: false,
      },
    },
  };

  //User info
  totalUsers
    = {
      header: '',
      chartValue: 0,
      number: 0,
    }

  totalImams
    = {
      header: '',
      chartValue: 0,
      number: 0,
    }
  totalAdmins
    = {
      header: '',
      chartValue: 0,
      number: 0,
    }
  ImamPending
    = {
      header: '',
      chartValue: 0,
      number: 0,
    }

  //Masjid info
  totalMasajid

    = {
      header: '',
      chartValue: 0,
      number: 0,
    }

  totalMasajidPending

    = {
      header: '',
      chartValue: 0,
      number: 0,
    }
  totalSalah

    = {
      header: '',
      chartValue: 0,
      number: 0,
    }
  totalCommonSalah

    = {
      header: '',
      chartValue: 0,
      number: 0,
    }

  //Download  info
  androidDownloads
    = {
      header: '',
      chartValue: 0,
      number: 0,
    }

  iosDownloads

    = {
      header: '',
      chartValue: 0,
      number: 0,
    }

  //  charts data
  data = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [{
      data: [10, 30, 20, 50, 40, 20, 90],
      label: 'iOS',
      backgroundColor: 'rgba(140,217,201,0.5)',
      borderColor: 'rgba(130,225,202,0.5)',
    }, {
      data: [18, 48, 77, 9, 100, 27, 40],
      label: 'Android',
      backgroundColor: 'rgba(188,206,236,0.7)',
      borderColor: 'rgba(201,212,234,0.7)',
    }
    ],
  };

  options = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [
        {
          gridLines: {
            display: true,
            color: 'rgba(0,0,0,0.3)',
          },
          ticks: {
            fontColor: 'rgba(0,0,0,0.7)',
          },
        },
      ],
      yAxes: [
        {
          gridLines: {
            display: true,
            color: 'rgba(0,0,0,0.3)',
          },
          ticks: {
            fontColor: 'rgba(0,0,0,0.7)',
          },
        },
      ],
    },
    legend: {
      labels: {
        fontColor: 'rgba(0,0,0,0.7)',
      },
    },
  };

  imamReqSettings = {
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: true,
      custom: [],
      position: 'right'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // profilepicture: {
      //   title: 'Profile Picture',
      //   type: 'string',
      //   filter: false,
      // },
      name: {
        title: 'Name',
        type: 'string'
      },
      masjidName: {
        title: 'Masjid Name',
        type: 'string'
      },
      email: {
        title: 'Email',
        type: 'string'
      },
      phone: {
        title: 'Phone',
        type: 'string'
      },
      userAddress: {
        title: 'User Address',
        type: 'string',
      },
      masjidAddress: {
        title: 'Masjid Address',
        type: 'string',
      },
      verification: {
        title: 'Verification',
        type: 'custom',
        renderComponent: CustomRenderComponent,
        filter: false,
      },
    },
  };
  masjidTableSettings = {
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: true,
      custom: [],
      position: 'right',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: 'Masjid Name',
        type: 'string',
      },
      address: {
        title: 'Address',
        type: 'string',
      },
      city: {
        title: 'City',
        type: 'string',
      },
      state: {
        title: 'State',
        type: 'string',
      },
      country: {
        title: 'Country',
        type: 'string',
      },
      zipCode: {
        title: 'Zip Code',
        type: 'string',
      },
      suggestedBy: {
        title: 'Suggested By',
        type: 'string',
      },
      confirm: {
        title: 'Confirm',
        type: 'custom',
        renderComponent: CustomRenderComponent,
        filter: false,
      },
    },
  };

  userTableSource: LocalDataSource = new LocalDataSource();
  masjidTableSource: LocalDataSource = new LocalDataSource();
  transferTableSource: LocalDataSource = new LocalDataSource();

  onDeleteConfirmMasjid(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      if (event.data.imamRef) {
        this.masjidService.deleteMasajidWithImam(event.data.id, event.data.imamRef.id);
      } else {
        this.masjidService.deleteMasajid(event.data.id);
      }

    } else {
      event.confirm.reject();
    }
  }

  onDeleteConfirmImam(event): void {
    if (window.confirm('Are you sure you want to remove this admin?')) {
      event.confirm.resolve();
      this.userService.deleteImamReq(event.data.requestID);
    } else {
      event.confirm.reject();
    }
  }

  constructor(
    private userService: UserService,
    private salahService: SalahService,
    private masjidService: MasajidService,
  ) { }

  pendingMasjidSource: LocalDataSource = new LocalDataSource();
  unverifiedMasajid: any[] = [];
  imamReqCollection: { id: string; newImam: User; newMasjid: Masjid }[] = [];
  adminApprovalSource: LocalDataSource;


  ngOnInit() {

    this.masjidService.getRequestedMasajid()
      .subscribe(
        (value: Promise<{ data: Masjid, id: string, suggestedBy?: User }>[]) => {
          this.unverifiedMasajid = [];
          this.pendingMasjidSource = new LocalDataSource();
          value.forEach((dataset) => {
            const datas = {
              name: '',
              address: '',
              city: '',
              state: '',
              country: '',
              zipCode: '',
              suggestedBy: '',
              id: '',
            };
            dataset.then(
              (dataWithUser) => {

                if (dataWithUser.data.name) { datas.name = dataWithUser.data.name; }
                if (dataWithUser.data.address) { datas.address = dataWithUser.data.address; }
                if (dataWithUser.data.city) { datas.city = dataWithUser.data.city; }
                if (dataWithUser.data.state) { datas.state = dataWithUser.data.state; }
                if (dataWithUser.data.country) { datas.country = dataWithUser.data.country; }
                if (dataWithUser.data.zipCode) { datas.zipCode = dataWithUser.data.zipCode; }
                if (dataWithUser.suggestedBy && dataWithUser.suggestedBy.name)
                { datas.suggestedBy = dataWithUser.suggestedBy.name.firstName +
                  ' ' + dataWithUser.suggestedBy.name.lastName; }
                datas.id = dataWithUser.id;

                this.unverifiedMasajid.push(Object.assign(datas))
              },
            ).then(
              () => {
                this.pendingMasjidSource = new LocalDataSource(this.unverifiedMasajid);
              },
            )
              .catch(
                (dataNoUser) => {
                  console.log(dataNoUser);
                  if (dataNoUser.data.name) { datas.name = dataNoUser.data.name; }
                  if (dataNoUser.data.address) { datas.address = dataNoUser.data.address; }
                  if (dataNoUser.data.city) { datas.city = dataNoUser.data.city; }
                  if (dataNoUser.data.state) { datas.state = dataNoUser.data.state; }
                  if (dataNoUser.data.country) { datas.country = dataNoUser.data.country; }
                  if (dataNoUser.data.zipCode) { datas.zipCode = dataNoUser.data.zipCode; }
                  datas.id = dataNoUser.id;

                  this.unverifiedMasajid.push(Object.assign(datas));
                },
              ).then(
                () => {
                this.pendingMasjidSource = new LocalDataSource(this.unverifiedMasajid);
                },
              );
          });
        }
      );

      this.userService.getImamReq()
      .subscribe(
        (value: { id: string; imamPromise: Promise<User>;
          masjidPromise: Promise<Masjid>; masjidID: string;  imamID: string }[]) => {
          this.imamReqCollection = [];
          value.forEach(dataset => {
            let newImam: User;
            let newMasjid: Masjid;
            let id: string;
            let masjidId: string;
            let imamID: string;
            dataset.imamPromise
              .then((imam) => newImam = imam)
              .then(() => {
                dataset.masjidPromise.then(
                  (masjid) => {
                    newMasjid = masjid;
                    id = dataset.id;
                    masjidId = dataset.masjidID;
                    imamID = dataset.imamID;
                  }
                )
                  .then(() => {
                    const newDataset = { id, newImam, newMasjid, masjidId, imamID};
                    const datas = {
                      name: '',
                      masjidName: '',
                      email: '',
                      phone: '',
                      userAddress: '',
                      masjidAddress: '',
                      userID: '',
                      requestID: '',
                      masjidID: '',
                    };

                    datas.userID = imamID;
                    datas.requestID = id;
                    datas.masjidID = masjidId;

                    if (newDataset.newImam && newDataset.newImam.name) {
                      datas.name = newImam.name.firstName + ' ' + newImam.name.lastName;
                    }
                    if (newDataset.newMasjid && newDataset.newMasjid.name) {
                      datas.masjidName = newMasjid.name;
                    }
                    if (newDataset.newMasjid && newDataset.newMasjid.address) {
                      datas.masjidAddress = newMasjid.address;
                    }
                    if (newDataset.newImam && newDataset.newImam.email) {
                      datas.email = newImam.email;
                    }
                    if (newDataset.newImam && newDataset.newImam.phone) {
                      datas.phone = newImam.phone;
                    }
                    if (newDataset.newImam && newDataset.newImam.address) {
                      datas.userAddress = newImam.address;
                    }
                    this.imamReqCollection.push(Object.assign(datas));

                  }).then(
                    () => {
                      this.adminApprovalSource = new LocalDataSource(this.imamReqCollection);
                      console.log(this.imamReqCollection);
                    },
                  );
              });
          });
        },
      );

    this.userService
      .getUser()
      .subscribe((value) => {
        this.totalUsers.number = 0;
        this.totalImams.number = 0;
        this.totalAdmins.number = 0;
        value.forEach(dataset => {

          if (dataset.data.roles && dataset.data.roles.imam) {
            this.totalImams.number += 1;
          }

          if (dataset.data.roles && dataset.data.roles.admin) {
            this.totalAdmins.number += 1;
          }
          this.totalUsers.number += 1;

        });
      });

      this.userService
      .getImamReq()
      .subscribe((value) => {
        this.ImamPending.number = 0;
        value.forEach(dataset => {
          this.ImamPending.number += 1;
        });
      });

      this.salahService
      .getSalah()
      .subscribe((value) => {
        this.totalSalah.number = 0;
        value.forEach(dataset => {
          this.totalSalah.number += 1;
        });
      });

      this.salahService
      .getCommonSalah()
      .subscribe((value) => {
        this.totalCommonSalah.number = 0;
        value.forEach(dataset => {
          this.totalCommonSalah.number += 1;
        });
      });

      this.masjidService
      .getMasjid()
      .subscribe((value) => {
        this.totalMasajid.number = 0;
        value.forEach(dataset => {
          this.totalMasajid.number += 1;
        });
      });

      this.masjidService
      .getRequestedMasajid()
      .subscribe((value) => {
        this.totalMasajidPending.number = 0;
        value.forEach(dataset => {
          this.totalMasajidPending.number += 1;
        });
      });


    this.totalUsers.header = 'Total Users';
    this.totalUsers.chartValue = 86;
    this.totalImams.header = 'Total Imams';
    this.totalImams.chartValue = 15;
    this.totalAdmins.header = 'Total Admins';
    this.totalAdmins.chartValue = 60;
    this.totalSalah.header = 'Total Salah';
    this.totalSalah.chartValue = 25;
    this.totalCommonSalah.header = 'Total Common Salah';
    this.totalCommonSalah.chartValue = 100;
    this.totalMasajid.header = 'Total Masjid';
    this.totalMasajid.chartValue = 100;
    this.totalMasajidPending.header = 'Masjid Pending';
    this.totalMasajidPending.chartValue = 59;

    this.ImamPending.header = 'Imam Pending';
    this.ImamPending.chartValue = 42;


    this.androidDownloads.header = 'Android Downloads';
    this.androidDownloads.chartValue = 91;
    this.androidDownloads.number = 19;
    this.iosDownloads.header = 'iOS Downloads';
    this.iosDownloads.chartValue = 0;
    this.iosDownloads.number = 0;
  }

  ngOnDestroy() {
    // this.alive = false;
  }
}
