import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { TablesRoutingModule, routedComponents } from './salah-routing.module';
import { SalahComponent } from './salah.component';
import { SmartTableComponent } from './smart-table/smart-table.component';
import { AddSalahComponent } from './add-salah/add-salah.component';
import { DatepickerComponent } from './datepicker/datepicker.component';


@NgModule({
  imports: [
    ThemeModule,
    TablesRoutingModule,
    Ng2SmartTableModule,

  ],
  declarations: [
    SmartTableComponent,
    SalahComponent,
    AddSalahComponent,
    DatepickerComponent,

  ],
  // exports: [
  //   SmartTableComponent,
  //   UsersComponent,
  //   AddUserComponent,
  //   MasjidAdminTableComponent,
  // ],
})
export class SalahModule {
}
