import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';
import { Salah } from '../../../models/salah.model';
import { SalahService } from '../salah.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { MasajidService } from '../../masjids/masajid.service';
import { Masjid } from '../../../models/masjid.model';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-form-inputs',
  styleUrls: ['./add-salah.component.scss'],
  templateUrl: './add-salah.component.html',
})
export class AddSalahComponent implements OnInit{

  starRate = 2;
  heartRate = 4;
  hours = '';
  minutes = '';
  radioGroupValue = 'This is value 2';
  masajidObservable$: Subscription;
  verifiedMasajid: any[] = [];
  newUsersArray: any[] = [];
  arraySalah: Salah = { name: '',
  time: { startTime: null },
  repeat: {
    sunday: true,
    monday: true,
    tuesday: true,
    wednesday: true,
    thursday: true,
    friday: true,
    saturday: true,

  },
  masjidRef: null,
  date: {
    startDate: null,
    endDate: null,
  },
};

  repeat = {
    sun: true,
    mon: true,
    tue: true,
    wed: true,
    thu: true,
    fri: true,
    sat: true,
  };

constructor(
  private salahService: SalahService,
  private afs: AngularFirestore,
  private masajidService: MasajidService,
  private router: Router,
) {}

  ngOnInit() {
    this.masajidService
    .getVerifiedMasjids()
    .subscribe((value: { data: Masjid; id: string }[]) => {
      this.verifiedMasajid = [];
      value.forEach(dataset => {
        this.verifiedMasajid.push(dataset);
      });
    });

  }

  changeDate(input: string) {
    switch (input) {
        case 'mon':
        this.repeat.mon = !this.repeat.mon;
        break;
        case 'tue':
        this.repeat.tue = !this.repeat.tue;
        break;
        case 'wed':
        this.repeat.wed = !this.repeat.wed;
        break;
        case 'thu':
        this.repeat.thu = !this.repeat.thu;
        break;
        case 'fri':
        this.repeat.fri = !this.repeat.fri;
        break;
        case 'sat':
        this.repeat.sat = !this.repeat.sat;
        break;
        case 'sun':
        this.repeat.sun = !this.repeat.sun;
        break;
    }

  }

  onSubmitAdd(f: NgForm) {
    console.log(f);

    if (f.value.salah !== '') {
      this.arraySalah.name = f.value.salah;
    }
    if (f.value.stime !== null) {
      this.hours = f.value.stime.substring(0, 2);
      this.minutes = f.value.stime.substring(3, 5);
      const startTime = { hours: this.hours, minutes: this.minutes };
      this.arraySalah.time.startTime = startTime;
    }
    if (f.value.etime !== '') {
      this.hours = f.value.etime.substring(0, 2);
      this.minutes = f.value.etime.substring(3, 5);
      const endTimeObj = { hours: this.hours, minutes: this.minutes };
      this.arraySalah.time.endTime = endTimeObj;
    }
    if (f.value.m_name !== '') {
      const id = f.value.m_name;
    }

    this.arraySalah.date.startDate = f.value.sdate;
    this.arraySalah.date.endDate = f.value.edate;

    this.arraySalah.masjidRef = this.afs.doc('masajid/' + f.value.m_name).ref;

    this.arraySalah.repeat.monday = this.repeat.mon;
    this.arraySalah.repeat.tuesday = this.repeat.tue;
    this.arraySalah.repeat.wednesday = this.repeat.wed;
    this.arraySalah.repeat.thursday = this.repeat.thu;
    this.arraySalah.repeat.friday = this.repeat.fri;
    this.arraySalah.repeat.saturday = this.repeat.sat;
    this.arraySalah.repeat.sunday = this.repeat.sun;

    console.log(this.arraySalah);
    this.pushNewSalah(this.arraySalah);
  }

  pushNewSalah(newSalah: Salah) {
    this.salahService.addSalah(newSalah);
    this.router.navigate(['/pages/salah/view-salah']);
  }
}
