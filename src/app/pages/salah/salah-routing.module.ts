import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SalahComponent } from './salah.component';
import { SmartTableComponent } from './smart-table/smart-table.component';
import { AddSalahComponent } from './add-salah/add-salah.component';

const routes: Routes = [{
  path: '',
  component: SalahComponent,
  children: [{
    path: 'view-salah',
    component: SmartTableComponent,
  },
  {
    path: 'add-salah',
    component: AddSalahComponent,
  },
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TablesRoutingModule { }

export const routedComponents = [
  SalahComponent,
  SmartTableComponent,
];
