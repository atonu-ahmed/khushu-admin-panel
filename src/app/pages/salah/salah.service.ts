import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
  DocumentReference
} from '@angular/fire/firestore';
import { Subscription, Observable } from 'rxjs';
import { map, repeat } from 'rxjs/operators';
import { Masjid } from '../../models/masjid.model';
import { Salah } from '../../models/salah.model';
import { CommonSalah } from '../../models/commonSalah.model';

@Injectable({
  providedIn: 'root'
})
export class SalahService {

  constructor(private afs: AngularFirestore) { }
  masajidObservable$: Observable<{ data: Masjid; id: string }[]>;
  masajidCollection: AngularFirestoreCollection<Masjid> = this.afs.collection(
    'masajid'
  );
  salahrepeatObservable$: Observable<{ id: string; salahRepeat: any }[]>;
  salahObservable$: Observable<
    Promise<{ data: Salah; id: string; masjid?: Masjid; salahRepeat?: any }>[]
  >;
  salahCollection: AngularFirestoreCollection<Salah> = this.afs.collection(
    'salahs'
  );

  getSalahRepeat() {
    this.salahrepeatObservable$ = this.salahCollection.snapshotChanges().pipe(
      map(action =>
        action.map(a => {
          const id = a.payload.doc.id;
          let salahRepeat;
          this.salahCollection
            .doc(id)
            .collection('repeat')
            .doc('repeat')
            .valueChanges()
            .subscribe(doc => {
              console.log(doc);
              salahRepeat = doc;
            });
          return { id, salahRepeat };
        })
      )
    );
    return this.salahrepeatObservable$;

  }

  // getSalah() {
  //   let salahRepeat: any;
  //   this.salahObservable$ = this.salahCollection.snapshotChanges().pipe(
  //     map(action =>
  //       action.map(
  //         (a: { payload: { doc: { data: () => Salah; id: any } } }) => {
  //           const data = a.payload.doc.data() as Salah;
  //           const id = a.payload.doc.id;
  //           let masjidID: string;
  //           let masjid: Masjid;
  //           const repeatdata = this.salahCollection
  //             .doc(id)
  //             .collection('repeat')
  //             .doc('repeat')
  //             .ref.get();
  //           // .map(action => action.map(a1 => { const data = a1.payload.doc.data;}))
  //           const salahRepeatPromise: Promise<{ data: any }> = new Promise(
  //             (resolve, reject) => {
  //               if (repeatdata) {
  //                 repeatdata.then(doc => {
  //                   resolve(doc);
  //                 });
  //               } else {
  //                 reject(console.log('No data'));
  //               }
  //             }
  //           );
  //           function getSalahRepeat() {
  //             salahRepeatPromise
  //               .then(doc => {
  //                 salahRepeat = doc.data();
  //               })
  //               .catch(() => {
  //                 console.log('No Data');
  //               });
  //             return salahRepeatPromise;

  //           }

  //           const salahMosquePromise: Promise<{
  //             data: Salah;
  //             id: string;
  //             salahRepeat?: any;
  //             masjid?: Masjid;
  //           }> = new Promise((resolve, reject) => {
  //             getSalahRepeat().then(() => {
  //               if (data.mosqueRef) {
  //                 masjidID = data.mosqueRef.id;
  //                 this.afs
  //                   .doc(`masajid/${masjidID}`)
  //                   .ref.get()
  //                   .then(doc => {
  //                     masjid = doc.data() as Masjid;
  //                     // console.log(salahRepeat);
  //                     resolve({ data, id, salahRepeat, masjid });
  //                   });
  //               } else {
  //                 console.log(salahRepeat);
  //                 reject({ data, id, salahRepeat });
  //               }
  //             });
  //             // if (data.mosqueRef) {
  //             //   masjidID = data.mosqueRef.id;
  //             //   this.afs
  //             //     .doc(`masajid/${masjidID}`)
  //             //     .ref.get()
  //             //     .then(doc => {
  //             //       masjid = doc.data() as Masjid;
  //             //       console.log(salahRepeat);
  //             //       resolve({ data, id, salahRepeat, masjid });
  //             //     });
  //             // } else {
  //             //   console.log(salahRepeat);
  //             //   reject({ data, id, salahRepeat });
  //             // }
  //           });

  //           salahMosquePromise
  //             .then(
  //               (dataWithMasjid: {
  //                 data: Salah;
  //                 id: string;
  //                 salahRepeat: any;
  //                 masjid: Masjid;
  //               }) => console.log(dataWithMasjid)
  //             )
  //             .catch(
  //               (dataNoMasjid: { data: Salah; id: string; salahRepeat: any }) =>
  //                 console.log(dataNoMasjid)
  //             );

  //           return salahMosquePromise;


  //         }
  //       )
  //     )
  //   );

  //   return this.salahObservable$;
  // }

  commonSalahCollection: AngularFirestoreCollection<CommonSalah> = this.afs.collection(
    'commonSalahs'
  );

  getSalah() {
    let salahRepeat: any;
    this.salahObservable$ = this.salahCollection.snapshotChanges().pipe(
      map(action =>
        action.map(
          (a: { payload: { doc: { data: () => Salah; id: any } } }) => {
            const data = a.payload.doc.data() as Salah;
            const id = a.payload.doc.id;
            let masjidID: string;
            let masjid: Masjid;
            const repeatdata = this.salahCollection
              .doc(id)
              .collection('repeat')
              .doc('repeat')
              .ref.get();
            // .map(action => action.map(a1 => { const data = a1.payload.doc.data;}))
            const salahRepeatPromise: Promise<{ data: any }> = new Promise(
              (resolve, reject) => {
                if (repeatdata) {
                  repeatdata.then(doc => {
                    resolve(doc);
                  });
                } else {
                  reject(console.log('No data'));
                }
              }
            );
            function getSalahRepeat() {
              salahRepeatPromise
                .then(doc => {
                  salahRepeat = doc.data();
                })
                .catch(() => {
                  console.log('No Data');
                });
              return salahRepeatPromise;

            }

            const salahMosquePromise: Promise<{
              data: Salah;
              id: string;
              salahRepeat?: any;
              masjid?: Masjid;
            }> = new Promise((resolve, reject) => {
              getSalahRepeat().then(() => {
                if (data.masjidRef) {
                  masjidID = data.masjidRef.id;
                  this.afs
                    .doc(`masajid/${masjidID}`)
                    .ref.get()
                    .then(doc => {
                      masjid = doc.data() as Masjid;
                      console.log(salahRepeat);
                      resolve({ data, id, salahRepeat, masjid });
                    });
                } else {
                  console.log(salahRepeat);
                  reject({ data, id, salahRepeat });
                }
              });
              // if (data.masjidRef) {
              //   masjidID = data.masjidRef.id;
              //   this.afs
              //     .doc(`masajid/${masjidID}`)
              //     .ref.get()
              //     .then(doc => {
              //       masjid = doc.data() as Masjid;
              //       console.log(salahRepeat);
              //       resolve({ data, id, salahRepeat, masjid });
              //     });
              // } else {
              //   console.log(salahRepeat);
              //   reject({ data, id, salahRepeat });
              // }
            });

            salahMosquePromise
              .then(
                (dataWithMasjid: {
                  data: Salah;
                  id: string;
                  salahRepeat: any;
                  masjid: Masjid;
                }) => dataWithMasjid
              )
              .catch(
                (dataNoMasjid: { data: Salah; id: string; salahRepeat: any }) =>
                  dataNoMasjid
              );

            return salahMosquePromise;


          }
        )
      )
    );

    return this.salahObservable$;
  }
  deleteSalah(id: string) {

    const document = this.salahCollection.doc(`${id}`);
    document
      .delete()
      .then(() => {
        console.log(`Document of id ${id} deleted Successfully`);
      })
      .catch((error: any) => {
        console.log('Error Removing Document', error);
      });
  }

  addCommonSalah(newSalah: CommonSalah): void {
    this.commonSalahCollection
      .add(newSalah)
      .then((docRef: DocumentReference) => {
        console.log('Document id set as', docRef.id);
      })
      .catch(error => {
        console.log('Error Adding Document', error);
      });
  }

  deleteCommonSalah(id: string) {

    return this.afs.collection('commonSalahs').doc(id).delete()
    .then(() => console.log('done'))
    .catch((err) => console.log(err));
}

  getCommonSalah() {
    return this.afs.collection('commonSalahs').snapshotChanges()
      .pipe(
        map(action =>
          action.map(a => {
            const data = a.payload.doc.data() as CommonSalah;
            const id = a.payload.doc.id;
            return { data, id };
          }))
      );
  }
  getMasjid() {
    this.masajidObservable$ = this.masajidCollection.snapshotChanges().pipe(
      map(action =>
        action.map(a => {
          const data = a.payload.doc.data() as Masjid;
          const id = a.payload.doc.id;
          // const ref =
          return { data, id };
        })
      )
    );

    return this.masajidObservable$;
  }
  editSalah(salahData: Salah, salahId: string) {
    const salahRef: AngularFirestoreDocument<Salah> = this.afs.doc(
      `salahs/${salahId}`
    );

    salahRef
      .update(salahData)
      .then(() => {
        console.log(`successfully edited User`);
      })
      .catch(error => {
        console.error(error);
      });
  }

  editCommonSalah(salahData: CommonSalah, salahId: string) {
    const salahRef: AngularFirestoreDocument<CommonSalah> = this.afs.doc(
      `commonSalahs/${salahId}`
    );

    salahRef
      .set(salahData, { merge: true})
      .then(() => {
        console.log(`successfully edited User`);
      })
      .catch(error => {
        console.error(error);
      });
  }

  addSalah(newSalah: Salah): void {
    this.salahCollection
      .add(newSalah)
      .then((docRef: DocumentReference) => {
        console.log('Document id set as', docRef.id);
      })
      .catch(error => {
        console.log('Error Adding Document', error);
      });
  }
  // editSunday(
  //   salahId: string,
  //   sundays: boolean,
  //   mondays: boolean = false,
  //   tuesdays: boolean = false,
  //   wednesdays: boolean = false,
  //   thursdays: boolean = false,
  //   fridays: boolean = false,
  //   staturdays: boolean = false
  // ) {
  //   this.salahCollection
  //     .doc(salahId)
  //     .update({
  //       repeat: {
  //         sunday: sundays,
  //         monday: mondays,
  //         tuesday: tuesdays,
  //         wednesday: wednesdays,
  //         thursday: thursdays,
  //         friday: fridays,
  //         staturday: staturdays
  //       }
  //     })
  //     // tslint:disable-next-line: only-arrow-functions
  //     .then(function() {
  //       console.log('Document successfully updated!');
  //     })
  //     // tslint:disable-next-line: only-arrow-functions
  //     .catch(function(error) {
  //       console.log('Error found data not changed' + error);
  //     });
  // }
  checkstatus(status: boolean = true) {
    console.log(status);
    return !status;
  }
  editSunday(salahId: string, status: boolean) {
    this.salahCollection
      .doc(salahId)
      .collection('repeat')
      .doc('repeat')
      .set(
        {
          sunday: status
        },
        { merge: true }
      )
      .then(() => {
        console.log('Document successfully updated!');
        console.log(status);
      })
      .catch(error => {
        console.log('Error found data not changed' + error);
      });
  }
  editMonday(salahid: string, status: boolean) {
    this.salahCollection
      .doc(salahid)
      .collection('repeat')
      .doc('repeat')
      .set(
        {
          monday: status
        },
        { merge: true }
      )
      .then(() => {
        console.log('Document successfully updated!');
      })
      .catch(error => {
        console.log('Error found data not changed' + error);
      });
  }
  editTuesday(salahid: string, status: boolean) {
    this.salahCollection
      .doc(salahid)
      .collection('repeat')
      .doc('repeat')
      .set(
        {
          tuesday: status
        },
        { merge: true }
      )
      .then(() => {
        console.log('Document successfully updated!');
      })
      .catch(error => {
        console.log('Error found data not changed' + error);
      });
  }
  editWednesday(salahid: string, status: boolean) {
    this.salahCollection
      .doc(salahid)
      .collection('repeat')
      .doc('repeat')
      .set(
        {
          wednesday: status
        },
        { merge: true }
      )
      .then(() => {
        console.log('Document successfully updated!');
      })
      .catch(error => {
        console.log('Error found data not changed' + error);
      });
  }
  editThursday(salahid: string, status: boolean) {
    this.salahCollection
      .doc(salahid)
      .collection('repeat')
      .doc('repeat')
      .set(
        {
          thursday: status
        },
        { merge: true }
      )
      .then(() => {
        console.log('Document successfully updated!');
      })
      .catch(error => {
        console.log('Error found data not changed' + error);
      });
  }
  editFriday(salahid: string, status: boolean) {
    this.salahCollection
      .doc(salahid)
      .collection('repeat')
      .doc('repeat')
      .set(
        {
          friday: status
        },
        { merge: true }
      )
      .then(() => {
        console.log('Document successfully updated!');
      })
      .catch(error => {
        console.log('Error found data not changed' + error);
      });
  }
  editSaturday(salahid: string, status: boolean) {
    this.salahCollection
      .doc(salahid)
      .collection('repeat')
      .doc('repeat')
      .set(
        {
          saturday: status
        },
        { merge: true }
      )
      .then(() => {
        console.log('Document successfully updated!');
      })
      .catch(error => {
        console.log('Error found data not changed' + error);
      });
  }
}
