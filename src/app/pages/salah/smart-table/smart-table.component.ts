import { interval, Observable } from 'rxjs';
import { timeout } from 'rxjs/operators';

import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
  DocumentReference,
} from '@angular/fire/firestore';
import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Time } from '@angular/common';

import { SmartTableData } from '../../../@core/data/smart-table';
import { Subscription } from 'rxjs';
import { SalahService } from '../salah.service';
import { Masjid } from '../../../models/masjid.model';
import { Salah } from '../../../models/salah.model';
import { CommonSalah } from '../../../models/commonSalah.model';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { MasajidService } from '../../masjids/masajid.service';

@Component({
  selector: 'ngx-smart-table',
  templateUrl: './smart-table.component.html',
  styles: ['./smart-table.component.scss'],
})
export class SmartTableComponent implements OnInit {
  settings = {
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: true,
      custom: [],
      position: 'right',
    },

    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: 'Salah',
        type: 'string',
      },
      startTime: {
        title: 'Start Time',
        type: 'string',
      },
      endTime: {
        title: 'End Time',
        type: 'string',
      },
      startDate: {
        title: 'Start Date',
        type: 'string',
      },
      endDate: {
        title: 'End Date',
        type: 'string',
      },
      masjidName: {
        title: 'Masjid Name',
        type: 'string',
      },
    },
  };
  genericSalahssettings = {
    actions: {
      columnTitle: 'Actions',
      add: true,
      edit: true,
      delete: true,
      custom: [],
      position: 'right',
    },
    add: {
      inputClass: '',
      addButtonContent: 'Add Salah &nbsp; +',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: 'Salah',
        type: 'string',
      },
      wakt: {
        title: 'Wakt',
        type: 'string',
      },
      // endTime: {
      //   title: 'End Time',
      //   type: 'string',
      // },
      // masjidName: {
      //   title: 'Masjid Name',
      //   type: 'string',
      // },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  genericSalahsSource: LocalDataSource = new LocalDataSource();
  salahObservable$: Subscription;
  genericSalahs: Subscription;
  onlineSalah: {
    data: Salah;
    id: string;
    salahRepeat?: any;
    masjid?: Masjid;
  }[] = [];
  allSalah: any[];
  genericSalah: any[];
  modalView = false;
  salahEdit: any;
  verifiedMasajid: any[] = [];


  constructor(
    private salahService: SalahService,
    private db: AngularFirestore,
    private masajidService: MasajidService,
  ) { }
  ngOnInit(): void {
    this.salahObservable$ = this.salahService
      .getSalah()
      .subscribe(
        (
          value: Promise<{
            data: Salah;
            id: string;
            // salahRepeat?: any;
            masjid?: Masjid;
          }>[],
        ) => {
          //
          this.allSalah = [];

          value.forEach(dataset => {
            const datas = {
              name: '',
              startTime: '',
              endTime: '',
              masjidName: '',
              startDate: '',
              endDate: '',
              id: '',
            };
            dataset
              .then(dataWithMasjid => {
                if (dataWithMasjid.data) {
                  if (dataWithMasjid.data.name) {
                    datas.name = dataWithMasjid.data.name;
                  } else {
                    datas.name = '';
                  }
                }

                if (dataWithMasjid.id) { datas.id = dataWithMasjid.id; }

                if (dataWithMasjid.masjid.name) {
                  datas.masjidName = dataWithMasjid.masjid.name;
                } else {
                  datas.masjidName = '';
                }

                if (dataWithMasjid.data.time) {
                  if (dataWithMasjid.data.time.startTime) {
                    datas.startTime = dataWithMasjid.data.time.startTime.hours.toString()
                      + ':' + dataWithMasjid.data.time.startTime.minutes.toString();

                  } else {
                    datas.startTime = '';
                  }

                  if (dataWithMasjid.data.time.endTime) {
                    datas.endTime = dataWithMasjid.data.time.endTime.hours.toString()
                      + ':' + dataWithMasjid.data.time.endTime.minutes.toString();

                  } else {
                    datas.endTime = '';
                  }
                }

                if (dataWithMasjid.data.date) {
                  if (dataWithMasjid.data.date.startDate) {
                    datas.startDate = dataWithMasjid.data.date.startDate.toDate().toString();

                  } else {
                    datas.startDate = '';
                  }

                  if (dataWithMasjid.data.date.endDate) {
                    datas.endDate = dataWithMasjid.data.date.endDate.toDate().toString();

                  } else {
                    datas.endDate = '';
                  }
                }


                // tslint:disable-next-line: no-console
                console.log(datas);
                this.allSalah.push(Object.assign(datas));

              },
              ).then(
                () => {
                  this.source = new LocalDataSource(this.allSalah);
                  // tslint:disable-next-line: no-console
                  console.log(this.source);
                },
              )
              .catch(dataNoMasjid => {
                if (dataNoMasjid.data) {
                  if (dataNoMasjid.data.name) {
                    datas.name = dataNoMasjid.data.name;
                  } else {
                    datas.name = '';
                  }
                  datas.masjidName = '';
                  if (dataNoMasjid.data.time) {
                    if (dataNoMasjid.data.time.startTime) {
                      datas.startTime = dataNoMasjid.data.time.startTime.hours.toString()
                        + ':' + dataNoMasjid.data.time.startTime.minutes.toString();

                    } else {
                      datas.startTime = '';
                    }

                    if (dataNoMasjid.data.time.endTime) {
                      datas.endTime = dataNoMasjid.data.time.endTime.hours.toString()
                        + ':' + dataNoMasjid.data.time.endTime.minutes.toString();

                    } else {
                      datas.endTime = '';
                    }
                  }

                  if (dataNoMasjid.data.date) {
                    if (dataNoMasjid.data.date.startDate) {
                      datas.startDate = dataNoMasjid.data.date.startDate.toDate().toString();

                    } else {
                      datas.startDate = '';
                    }

                    if (dataNoMasjid.data.date.endDate) {
                      datas.endDate = dataNoMasjid.data.date.endDate.toDate().toString();

                    } else {
                      datas.endDate = '';
                    }
                  }
                }
                if (dataNoMasjid.id) { datas.id = dataNoMasjid.id; }


                // tslint:disable-next-line: no-console
                console.log(datas);
                this.allSalah.push(Object.assign(datas));

              });
          });

        },
      );

        this.masajidService
        .getVerifiedMasjids()
        .subscribe((value: { data: Masjid; id: string }[]) => {
          this.verifiedMasajid = [];
          value.forEach(dataset => {
            this.verifiedMasajid.push(dataset);
          });
        });


    this.genericSalahs = this.salahService.getCommonSalah()
      .subscribe(
        (value) => {
          this.genericSalah = [];
          value.forEach(element => {
            this.genericSalah.push(Object.assign(element.data, { id: element.id }));
          });
          console.log(this.genericSalah);
          this.genericSalahsSource.load(this.genericSalah);
        },
      );

  }
  onDeleteConfirmCommonSalah(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.salahService.deleteCommonSalah(event.data.id);

    } else {
      event.confirm.reject();
    }
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.salahService.deleteSalah(event.data.id);

    } else {
      event.confirm.reject();
    }
  }

  pushNewCommonSalah(newSalah: any) {
    this.salahService.addCommonSalah(newSalah.newData);
    console.log(newSalah.newData);
  }

  deleteGenericSalah(data: any) {
    console.log(data);
  }

  editSalah(event) {
    console.log(event);

    const data = {
      name: '',
      email: '',
      country: '',
      address: '',
      id: '',
      city: '',
      state: '',
      zipCode: '',
      photoURL: '',
    };

    data.id = event.data.id;
    if (event.data.address) {
      data.address = event.data.address;
    }

    if (event.data.country) {
      data.country = event.data.country;
    }

    if (event.data.email) {
      data.email = event.data.email;
    }

    if (event.data.name) {
      data.name = event.data.name;
    }

    if (event.data.city) {
      data.city = event.data.city;
    }
    if (event.data.state) {
      data.state = event.data.state;
    }
    if (event.data.zipCode) {
      data.zipCode = event.data.zipCode;
    }
    if (event.data.photoURL) {
      data.photoURL = event.data.photoURL;
    }

    this.salahEdit = Object.assign(data);

    this.modalToggle();
  }

  editCommonSalah(event) {

    const editSalah: CommonSalah = {
      name: '',
      wakt: '',
    };

    if (event.newData.name) {
      editSalah.name = event.newData.name;
    }
    if (event.newData.wakt) {
      editSalah.wakt = event.newData.wakt;
    }

      this.salahService.editCommonSalah(editSalah, event.data.id);
    }

    modalToggle() {
      this.modalView = !this.modalView;
    }

    saveEditedSalah() {
      // tslint:disable-next-line: no-console
      // this.salahService.editSalah(this.masjidEdit, this.masjidEdit.id);
    }

    repeat = {
      sun: true,
      mon: true,
      tue: true,
      wed: true,
      thu: true,
      fri: true,
      sat: true,
    };
    changeDate(input: string) {
      switch (input) {
          case 'mon':
          this.repeat.mon = !this.repeat.mon;
          break;
          case 'tue':
          this.repeat.tue = !this.repeat.tue;
          break;
          case 'wed':
          this.repeat.wed = !this.repeat.wed;
          break;
          case 'thu':
          this.repeat.thu = !this.repeat.thu;
          break;
          case 'fri':
          this.repeat.fri = !this.repeat.fri;
          break;
          case 'sat':
          this.repeat.sat = !this.repeat.sat;
          break;
          case 'sun':
          this.repeat.sun = !this.repeat.sun;
          break;
      }

    }
  }
