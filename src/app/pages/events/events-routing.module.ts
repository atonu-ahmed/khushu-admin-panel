import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventComponent } from './event.component';
import { SmartTableComponent } from './smart-table/smart-table.component';
import { AddEventComponent } from './add-event/add-events.component';

const routes: Routes = [{
  path: '',
  component: EventComponent,
  children: [{
    path: 'view-events',
    component: SmartTableComponent,
  },
  {
    path: 'add-event',
    component: AddEventComponent,
  },
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TablesRoutingModule { }

export const routedComponents = [
  EventComponent,
  SmartTableComponent,
];
