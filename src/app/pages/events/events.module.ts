import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { TablesRoutingModule, routedComponents } from './events-routing.module';
import { EventComponent } from './event.component';
import { SmartTableComponent } from './smart-table/smart-table.component';
import { AddEventComponent } from './add-event/add-events.component';


@NgModule({
  imports: [
    ThemeModule,
    TablesRoutingModule,
    Ng2SmartTableModule,

  ],
  declarations: [
    SmartTableComponent,
    SmartTableComponent,
    AddEventComponent,
    EventComponent,

  ],
  // exports: [
  //   SmartTableComponent,
  //   UsersComponent,
  //   AddUserComponent,
  //   MasjidAdminTableComponent,
  // ],
})
export class EventsModule {
}
