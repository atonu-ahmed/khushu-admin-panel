import { Component } from '@angular/core';

@Component({
  selector: 'ngx-form-inputs',
  styleUrls: ['./add-event.component.scss'],
  templateUrl: './add-events.component.html',
})
export class AddEventComponent {

  starRate = 2;
  heartRate = 4;
  radioGroupValue = 'This is value 2';
}
