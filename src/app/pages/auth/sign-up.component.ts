import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import * as firebase from 'firebase';
import { WindowService } from './window.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../../../environments/environment';
import { User } from '../../models/user.model';

export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})


export class SignUpComponent implements OnInit {
  data: Omit<User, 'uid'> = {
    roles: {
      imam: false,
      admin: true
    }
  };
  email: string;
  password: string;


  constructor(
    public auth: AuthService,
    public afAuth: AngularFireAuth
  ) { }

  ngOnInit() {
    firebase.initializeApp(environment._firebase);
    this.auth.phoneWindowRef();
  }

  sendLoginCode() {
    this.auth.sendLoginCode();
  }

  verifyLoginCode() {
    this.auth.verifyLoginCode(this.data);
  }
  onSubmitAdd(f) {

    console.log('Here in add');
    this.data.email = f.value.email;
    this.data.address = f.value.address;
    this.data.country = f.value.country;
    this.data.phone = f.value.phone;
    this.auth.phoneNumber.number = f.value.phone;
    console.log(this.data);
    f.reset();
  }

}
