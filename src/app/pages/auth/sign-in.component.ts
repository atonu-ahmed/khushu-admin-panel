import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit, OnDestroy {
  email: string;
  password: string;

  constructor(
    public auth: AuthService
  ) { }

  ngOnInit() {
  }
  ngOnDestroy() {
    // this.auth.accountcheckerDestroyer();
  }

}
