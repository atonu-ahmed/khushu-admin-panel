import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ThemeModule } from '../../@theme/theme.module';


import { AuthRoutingModule } from './auth-routing.module';
import { SignInComponent } from './sign-in.component';
import { SignUpComponent } from './sign-up.component';

@NgModule({
  declarations: [SignInComponent, SignUpComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ThemeModule,
  ]
})
export class AuthModule { }
