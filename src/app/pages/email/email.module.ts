import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { TablesRoutingModule, routedComponents } from './email-routing.module';
import { EmailComponent } from './email.component';
import { SmartTableComponent } from './smart-table/smart-table.component';
import { AddEmailComponent } from './add-email/add-email.component';
import { CustomRenderComponent } from './smart-table/button';


@NgModule({
  imports: [
    ThemeModule,
    TablesRoutingModule,
    Ng2SmartTableModule,

  ],
  entryComponents : [
    CustomRenderComponent,
  ],
  declarations: [
    SmartTableComponent,
    EmailComponent,
    AddEmailComponent,
    CustomRenderComponent,

  ],
  // exports: [
  //   SmartTableComponent,
  //   UsersComponent,
  //   AddUserComponent,
  //   emailAdminTableComponent,
  // ],
})
export class EmailModule {
}
