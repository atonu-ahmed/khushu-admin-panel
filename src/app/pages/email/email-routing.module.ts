import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmailComponent } from './email.component';
import { SmartTableComponent } from './smart-table/smart-table.component';
import { AddEmailComponent } from './add-email/add-email.component';

const routes: Routes = [{
  path: '',
  component: EmailComponent,
  children: [{
    path: 'inbox',
    component: SmartTableComponent,
  },
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TablesRoutingModule { }

export const routedComponents = [
  AddEmailComponent,
  SmartTableComponent,
];
