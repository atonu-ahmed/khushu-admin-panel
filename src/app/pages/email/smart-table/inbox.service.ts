import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { firestore } from 'firebase';
import { Observable } from 'rxjs';
import { Message } from '../../../models/message.model';

@Injectable({
  providedIn: 'root'
})
export class InboxService {

  constructor(
    private afs: AngularFirestore
  ) { }

  messagesCollection: AngularFirestoreCollection<Message> = this.afs.collection('messages',
  (query) => query.where('readByAdmin', '==', false));
  messageObservable$: Observable<{ data: Message; id: string }[]>;

  // getInbox() {
  //   return this.messagesCollection.snapshotChanges().pipe(
  //     map(action =>
  //       action.map(a => {
  //         const data = a.payload.doc.data() as Message;
  //         const id = a.payload.doc.id;
  //         const fromPromise: Promise<{ data: Message, id: string, from: User }> = new Promise((resolve, reject) => {
  //           const userID = data.from.id;
  //           this.afs.doc(`users/${userID}`).ref.get()
  //             .then(
  //               (doc) => {
  //                 const from = doc.data() as User;
  //                 resolve({ data, id, from });
  //               }
  //             )
  //             .catch(
  //               () => {
  //                 reject('error');
  //               }
  //             );
  //         });
  //         fromPromise.then(
  //           (messageFrom: { data: Message, id: string, from: User }) => messageFrom
  //         )
  //           .catch(
  //             (error) => error
  //           );
  //         return fromPromise;
  //       })
  //     )
  //   );
  // }

  getInbox() {

    this.messageObservable$ = this.messagesCollection.snapshotChanges().pipe(
      map(action =>
        action.map(a => {
          const data = a.payload.doc.data() as Message;
          const id = a.payload.doc.id;
          return { data, id };
        })
      )
    );

    return this.messageObservable$;


  }


  get timestamp() {
    return firestore.FieldValue.serverTimestamp();
  }
  // firestore.Timestamp.fromDate(new Date())
  replyMessage(newMessage: Message) {
    this.afs.collection(`messages`).add(Object.assign(newMessage, { timestamp: this.timestamp }));
  }

  archiveMessage(id: string) {
    this.afs.doc(`messages/${id}`).update({readByAdmin: true});
  }
}
