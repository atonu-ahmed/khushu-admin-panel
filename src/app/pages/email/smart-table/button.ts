import { Component, Input, OnInit } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';
import { InboxService } from './inbox.service';

@Component({
  template:
    `<div style="width: 100%" class="d-flex justify-content-center">
    <button (click)="trigger()" class="yes-button mx-1">Archive</button>`
  ,
  styleUrls: ['./button.css'],
})
export class CustomRenderComponent implements ViewCell, OnInit {

  renderValue: string;

  @Input() value: string | number;
  @Input() rowData: any;

  trigger() {
    this.inboxService.archiveMessage(this.rowData.id);
  }
  ngOnInit() {
  }

  constructor(
    private inboxService: InboxService,
  ) {}
}
