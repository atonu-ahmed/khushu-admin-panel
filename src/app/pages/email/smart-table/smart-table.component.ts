import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { CustomRenderComponent } from './button';

import { InboxService } from './inbox.service';
import { User } from '../../../models/user.model';
import { Observable, Subscription } from 'rxjs';
import { Message } from '../../../models/message.model';

@Component({
  selector: 'ngx-smart-table',
  templateUrl: './smart-table.component.html',
  styles: ['./smart-table.component.scss'],
})
export class SmartTableComponent implements OnInit {
  adminApproveTableSettings = {
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: false,
      custom: [],
      position: 'right',
    },
    columns: {
      name: {
        title: 'From',
        type: 'string',
      },
      message: {
        title: 'Message',
        type: 'string',
      },
      phone: {
        title: 'Phone',
        type: 'string',
      },
      email: {
        title: 'Email',
        type: 'string',
      },
      time: {
        title: 'Time',
        type: 'string',
      },
      approve: {
        title: 'Approve',
        filter: false,
        type: 'custom',
        renderComponent: CustomRenderComponent,
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  // inboxMessages$: any;
  // Observable<Promise<{ data: Message; id: string; from: User; }>[]>;
  // messageSubscription: Subscription;
  // messages: { data: Message, id: string, from: User }[] = [];
  constructor(private inboxService: InboxService) {}
  // messageObservable$: Observable<{ data: Message; id: string }[]>;

  messages: Message[];
  ngOnInit() {

    this.inboxService.getInbox().subscribe(
      (value) => {
        console.log(value);
        this.messages = [];
        value.forEach(messageFromUser => {
          let datas = {
            name: '',
            message: '',
            phone: '',
            email: '',
            time: null,
            id: '',
          };
              if ( messageFromUser.data.name) {datas.name = messageFromUser.data.name; }
              if (messageFromUser.data.body) {datas.message = messageFromUser.data.body; }
              if (messageFromUser.data.phone) {datas.phone = messageFromUser.data.phone; }
              if (messageFromUser.data.email) {datas.email = messageFromUser.data.email; }
              if (messageFromUser.data.timestamp) {datas.time = messageFromUser.data.timestamp.toDate().toString(); }
              datas.id = messageFromUser.id;
          this.messages.push(Object.assign(datas));
        });
        console.log(this.messages);
        this.source = new LocalDataSource(this.messages);
      }
    );

    // console.log(this.messageObservable$);
    // this.inboxMessages$ = this.inboxService.getInbox();

    // this.messageSubscription = this.inboxMessages$.subscribe(
    //   (value: Promise<{ data: Message; id: string; from: User; }>[]) => {
    //     this.messages = [];
    //     this.source = new LocalDataSource();
    //     value.forEach(dataset => {
    //       let datas = {
    //         name: '',
    //         message: '',
    //         phone: '',
    //         email: '',
    //         time: null,
    //         id: '',
    //       };
    //       dataset.then(
    //         (messageFromUser) => {
    //           console.log(messageFromUser)
    //           if (messageFromUser.from && messageFromUser.from.name) {datas.name = messageFromUser.from.name.firstName+ ' '+ messageFromUser.from.name.lastName}
    //           if (messageFromUser.from && messageFromUser.from.phone) {datas.phone = messageFromUser.from.phone}
    //           if (messageFromUser.from && messageFromUser.from.email) {datas.email = messageFromUser.from.email}
    //           if (messageFromUser.data && messageFromUser.data.timestamp) {datas.time = messageFromUser.data.timestamp.toDate().toString()}
    //           datas.id = messageFromUser.id;
    //           this.messages.push(Object.assign(datas));
    //         }
    //       ).then(() => {
    //         this.source = new LocalDataSource(this.messages);
    //         console.log(this.messages);
    //       })
    //         .catch(
    //           (data) => console.log(data),
    //         );
    //     });
    //     console.log(this.messages);
    //   }
    // );
  }

  archiveMessage(id: string) {
    this.inboxService.archiveMessage(id);
  }
}
