import { Component } from '@angular/core';

@Component({
  selector: 'ngx-form-inputs',
  styleUrls: ['./add-email.component.scss'],
  templateUrl: './add-email.component.html',
})
export class AddEmailComponent {

  starRate = 2;
  heartRate = 4;
  radioGroupValue = 'This is value 2';
}
