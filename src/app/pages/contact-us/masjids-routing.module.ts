import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from './masjids.component';
import { SmartTableComponent } from './smart-table/smart-table.component';
import { AddMasjidComponent } from './add-masjid/add-masjids.component';
import { MasjidAdminTableComponent } from './masjid-admin/masjid-admin-table.component';

const routes: Routes = [{
  path: '',
  component: UsersComponent,
  children: [{
    path: 'view-masjids',
    component: SmartTableComponent,
  },
  {
    path: 'add-masjid',
    component: AddMasjidComponent,
  },
  {
    path: 'masjid-admins',
    component: MasjidAdminTableComponent,
  },
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TablesRoutingModule { }

export const routedComponents = [
  UsersComponent,
  SmartTableComponent,
];
