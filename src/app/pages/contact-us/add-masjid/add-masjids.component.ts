import { Component } from '@angular/core';

@Component({
  selector: 'ngx-form-inputs',
  styleUrls: ['./add-masjids.component.scss'],
  templateUrl: './add-masjids.component.html',
})
export class AddMasjidComponent {

  starRate = 2;
  heartRate = 4;
  radioGroupValue = 'This is value 2';
}
