import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Masjid } from '../../../models/masjid.model';
import { MasajidService } from '../masajid.service';
import { firestore } from 'firebase';
import { DocumentRef } from '@agm/core/utils/browser-globals';
import { Router, Route } from '@angular/router';

@Component({
  selector: 'ngx-form-inputs',
  styleUrls: ['./add-masjids.component.scss'],
  templateUrl: './add-masjids.component.html',
})
export class AddMasjidComponent implements OnInit{

  starRate = 2;
  heartRate = 4;
  radioGroupValue = 'This is value 2';
  data: Masjid = {
    name: '',
    location: new firestore.GeoPoint(90, 12),
    status: false,
    verified: true,
    address: '',
    email: '',
    zipCode: '',
    city: '',
    state: '',
    country: '',
    photoURL: '',
  };

  constructor(
    private masajidService: MasajidService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  onSubmitAdd() {
    this.data.status = true;
    this.pushNewMasjid(this.data);
  }

  pushNewMasjid(data: Masjid) {
    this.masajidService.addMasjid(data);
    this.router.navigate(['/pages/masjids/view-masjids']);
  }
}
