import { TestBed } from '@angular/core/testing';

import { ModalTogglerService } from './modal-toggler.service';

describe('ModalTogglerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModalTogglerService = TestBed.get(ModalTogglerService);
    expect(service).toBeTruthy();
  });
});
