import { Component, Input, OnInit } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';
import { MasajidService } from '../masajid.service';
import { ModalTogglerService } from './modal-toggler.service';

@Component({
  template:
    `<div style="width: 100%" class="d-flex justify-content-center">
    <button *ngIf="value"  (click)="update()" class="active-button mx-1">ACTIVE</button>
    <button *ngIf="!value" (click)="update()" class="disabled-button mx-1">DISABLED</button>
    `
  ,
  styleUrls: ['./button.css'],
})
export class ActivateButton implements ViewCell, OnInit {

  renderValue: string;

  @Input() value: string | number;
  @Input() rowData: any;

  update() {
    this.modalToggler.toggle = !this.modalToggler.toggle;
    const status = this.masajidService.checkStatus(this.rowData.status);
    this.masajidService.editStatus(this.rowData.id, status);
  }

  updateStatus(masjidId: string, status: boolean) {
    status = this.masajidService.checkStatus(status);
    this.masajidService.editStatus(masjidId, status);
  }

  constructor(
    private masajidService: MasajidService,
    private modalToggler: ModalTogglerService,
  ) {}
  ngOnInit() {
  }

}
