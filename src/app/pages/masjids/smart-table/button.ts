import { Component, Input, OnInit } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';
import { MasajidService } from '../masajid.service';

@Component({
  selector: 'ngx-button',
  template:
    `<div style="width: 100%" class="d-flex justify-content-center">
    <button  (click)="update()" class="active-button mx-1">CONFIRM</button>`
  ,
  styleUrls: ['./button.css'],
})
export class CustomRenderComponent implements ViewCell, OnInit {

  renderValue: string;

  @Input() value: string | number;
  @Input() rowData: any;

  update() {
    const verified = this.masajidService.checkStatus(this.rowData.verified);
    this.masajidService.editVerification(this.rowData.id, verified);
  }
  constructor(
    private masajidService: MasajidService,
  ) {}
  ngOnInit() {
  }

}
