import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { NbDialogService } from '@nebular/theme';


import { ViewCell } from 'ng2-smart-table';

@Component({
  templateUrl: './viewButton.html',
  styleUrls: ['./viewUser.css'],
})
export class ViewButtonComponent implements ViewCell, OnInit {

  renderValue: string;
  modalOpen = false;

  @Input() value: string | number;
  @Input() rowData: any;

  constructor(private dialogService: NbDialogService) { }


  ngOnInit() {
    this.renderValue = this.value.toString().toUpperCase();
  }

  open(dialog: TemplateRef<any>) {
    this.dialogService.open(
      dialog,
      {
        context: 'User information',
        closeOnEsc: true,
      });
  }
  openModal() {
    this.modalOpen = !this.modalOpen;
  }

}
