import { Component, OnInit, TemplateRef } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { CustomRenderComponent } from './button';
import { ActivateButton } from './activateButton';

import { SmartTableData } from '../../../@core/data/smart-table';
import { NbDialogService } from '@nebular/theme';
import { ViewButtonComponent } from './viewButton';
import { Subscription, Observable } from 'rxjs';
import { MasajidService } from '../masajid.service';
import { Masjid } from '../../../models/masjid.model';
import { User } from '../../../models/user.model';
import { DataSource } from 'ng2-smart-table/lib/data-source/data-source';
import { promise } from 'selenium-webdriver';
import { UserService } from '../../users/user.service';
import { ProfilePicComponent } from '../../../profile-pic/profile-pic.component';
import { ModalTogglerService } from './modal-toggler.service';

@Component({
  selector: 'ngx-smart-table',
  templateUrl: './smart-table.component.html',
  styles: ['./smart-table.component.scss'],
})
export class SmartTableComponent implements OnInit {
  settings = {
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: true,
      custom: [],
      position: 'right',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      photoURL: {
        title: 'Profile Picture',
        type: 'custom',
        filter: false,
        renderComponent: ProfilePicComponent,
      },
      name: {
        title: 'Masjid Name',
        type: 'string',
      },
      email: {
        title: 'E-Mail',
        type: 'string',
      },
      address: {
        title: 'Address',
        type: 'string',
      },
      city: {
        title: 'City',
        type: 'string',
      },
      state: {
        title: 'State',
        type: 'string',
      },
      country: {
        title: 'Country',
        type: 'string',
      },
      zipCode: {
        title: 'Zip Code',
        type: 'string',
      },
      admin: {
        title: 'Admin',
        type: 'string',
      },
      status: {
        title: 'Status',
        type: 'custom',
        renderComponent: ActivateButton,
        filter: false,
      },
    },
  };
  adminApproveTableSettings = {
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: true,
      custom: [],
      position: 'right',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      photoURL: {
        title: 'Profile Picture',
        type: 'custom',
        filter: false,
        renderComponent: ProfilePicComponent,
      },
      name: {
        title: 'Masjid Name',
        type: 'string',
      },
      address: {
        title: 'Address',
        type: 'string',
      },
      city: {
        title: 'City',
        type: 'string',
      },
      state: {
        title: 'State',
        type: 'string',
      },
      country: {
        title: 'Country',
        type: 'string',
      },
      zipCode: {
        title: 'Zip Code',
        type: 'string',
      },
      suggestedBy: {
        title: 'Suggested By',
        type: 'string',
      },
      confirm: {
        title: 'Confirm',
        type: 'custom',
        renderComponent: CustomRenderComponent,
        filter: false,
      },
    },
  };

  adminApproveTableSource: LocalDataSource = new LocalDataSource();
  source: LocalDataSource = new LocalDataSource();
  verifiedSubscription: Subscription;
  unverifiedSubscription: Subscription;
  usersObservable$: Subscription;
  modalView = false;

  verifiedMasajid: any[] = [];
  unverifiedMasajid: any[] = [];
  newUsersArray: any[] = [];
  imamsWithMasjid: any[] = [];
  masjidEdit: any;


  constructor(
    private service: SmartTableData,
    private masajidService: MasajidService,
    private userService: UserService,
    private dialogService: NbDialogService,
    public modalToggler: ModalTogglerService,
  ) { }
  ngOnInit(): void {
    this.userService
      .getUser()
      .subscribe((value: { data: User; id: string }[]) => {
        this.newUsersArray = [];

        value.forEach(dataset => {
          this.newUsersArray.push(Object.assign(dataset.data));
        });

        this.unverifiedSubscription = this.masajidService
          .getVerifiedMasjids()
          .subscribe((value: { data: Masjid; id: string }[]) => {
            this.verifiedMasajid = [];
            const admin = {
              admin: '',
            };
            value.forEach(dataset => {
              if (dataset.data.imamRef) {
                this.newUsersArray.forEach(element => {
                  if (dataset.data.imamRef.id === element.uid) {
                    admin.admin = element.name.firstName + ' ' + element.name.lastName;
                  }
                });
              }
              this.verifiedMasajid.push(Object.assign(dataset.data, { admin: admin.admin }, { id: dataset.id }));
              admin.admin = '';
            });
            this.source = new LocalDataSource(this.verifiedMasajid);
            console.log(this.verifiedMasajid)
          });
      });

    this.unverifiedSubscription = this.masajidService.getRequestedMasajid()
      .subscribe(
        (value: Promise<{ data: Masjid, id: string, suggestedBy?: User }>[]) => {
          this.unverifiedMasajid = [];
          this.adminApproveTableSource = new LocalDataSource();
          value.forEach((dataset) => {
            const datas = {
              name: '',
              address: '',
              city: '',
              state: '',
              country: '',
              zipCode: '',
              suggestedBy: '',
              id: '',
              photoURL: '',
            };
            dataset.then(
              (dataWithUser) => {

                if (dataWithUser.data.name) { datas.name = dataWithUser.data.name; }
                if (dataWithUser.data.address) { datas.address = dataWithUser.data.address; }
                if (dataWithUser.data.city) { datas.city = dataWithUser.data.city; }
                if (dataWithUser.data.state) { datas.state = dataWithUser.data.state; }
                if (dataWithUser.data.country) { datas.country = dataWithUser.data.country; }
                if (dataWithUser.data.zipCode) { datas.zipCode = dataWithUser.data.zipCode; }
                if (dataWithUser.data.photoURL) { datas.photoURL = dataWithUser.data.photoURL; }
                if (dataWithUser.suggestedBy && dataWithUser.suggestedBy.name)
                { datas.suggestedBy = dataWithUser.suggestedBy.name.firstName +
                  ' ' + dataWithUser.suggestedBy.name.lastName; }
                datas.id = dataWithUser.id;

                this.unverifiedMasajid.push(Object.assign(datas))
              },
            ).then(
              () => {
                console.log(this.unverifiedMasajid);
                this.adminApproveTableSource = new LocalDataSource(this.unverifiedMasajid);
              },
            )
              .catch(
                (dataNoUser) => {
                  console.log(dataNoUser);
                  if (dataNoUser.data.name) { datas.name = dataNoUser.data.name; }
                  if (dataNoUser.data.address) { datas.address = dataNoUser.data.address; }
                  if (dataNoUser.data.city) { datas.city = dataNoUser.data.city; }
                  if (dataNoUser.data.state) { datas.state = dataNoUser.data.state; }
                  if (dataNoUser.data.country) { datas.country = dataNoUser.data.country; }
                  if (dataNoUser.data.photoURL) { datas.photoURL = dataNoUser.data.photoURL; }
                  if (dataNoUser.data.zipCode) { datas.zipCode = dataNoUser.data.zipCode; }
                  datas.id = dataNoUser.id;

                  this.unverifiedMasajid.push(Object.assign(datas));
                },
              ).then(
                () => {
                console.log('catch');
                this.adminApproveTableSource = new LocalDataSource(this.unverifiedMasajid);
                },
              );
          });
        }
      );

  }
  modalToggle() {
    this.modalToggler.toggle = !this.modalToggler.toggle;
  }

  editMasjid(event) {
    console.log(event);

    const data = {
      name: '',
      email: '',
      country: '',
      address: '',
      id: '',
      city: '',
      state: '',
      zipCode: '',
      photoURL: '',
    };

    data.id = event.data.id;
    if (event.data.address) {
      data.address = event.data.address;
    }

    if (event.data.country) {
      data.country = event.data.country;
    }

    if (event.data.email) {
      data.email = event.data.email;
    }

    if (event.data.name) {
      data.name = event.data.name;
    }

    if (event.data.city) {
      data.city = event.data.city;
    }
    if (event.data.state) {
      data.state = event.data.state;
    }
    if (event.data.zipCode) {
      data.zipCode = event.data.zipCode;
    }
    if (event.data.photoURL) {
      data.photoURL = event.data.photoURL;
    }

    this.masjidEdit = Object.assign(data);

    // this.modalToggle();
    this.modalToggler.toggle = !this.modalToggler.toggle;
  }

  saveEditedUser() {
    // tslint:disable-next-line: no-console
    this.masajidService.editMasjid(this.masjidEdit, this.masjidEdit.id);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      if (event.data.imamRef) {
        this.masajidService.deleteMasajidWithImam(event.data.id, event.data.imamRef.id);
      } else {
        this.masajidService.deleteMasajid(event.data.id);
      }

    } else {
      event.confirm.reject();
    }
  }


  update(event): void { }
  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog, {
      context: 'this is some additional data passed to dialog',
      closeOnEsc: true,
    });
  }

  onCustom(event) {
    alert(`Custom event '${event.action}' fired on row №: ${event.data.id}`)
  }

  deleteMasjid(masjidRef: string, imamId: string) {
    console.log(masjidRef);
    if (imamId !== null) {
      this.masajidService.deleteMasajidWithImam(masjidRef, imamId);
    } else {
      this.masajidService.deleteMasajid(masjidRef);
    }

  }
}
