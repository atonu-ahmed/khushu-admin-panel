import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalTogglerService {

  constructor() { }
  public toggle = false;
}
