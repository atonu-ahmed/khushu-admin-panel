import { Injectable } from '@angular/core';
import {
  AngularFirestoreCollection,
  AngularFirestore,
  DocumentReference,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { firestore } from 'firebase';

import { Masjid } from '../../models/masjid.model';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class MasajidService {
  constructor(private afs: AngularFirestore) { }
  masajidCollection: AngularFirestoreCollection<Masjid> = this.afs.collection(
    'masajid',
  );
  // masajidVerifiedObservable$: Observable<{ data: Masjid; id: string }[]>;
  masjidObservable$: Observable<{ data: Masjid; id: string }[]>;
  verifiedMasjidObservable$: Observable<Promise<{ data: Masjid; id: string; imam?: User; }>[]>;
  unverifiedMasjidObservable$: Observable<Promise<{ data: Masjid; id: string; imam?: User; }>[]>;
  masajidObservable$: Observable<{ data: Masjid; id: string }[]>;
  verifiedMasjids$: Observable<any[]>;
  salah: Subscription;


  addMasjid(newMasajid: Masjid): void {
    this.masajidCollection
      .add(newMasajid)
      .then((docRef: DocumentReference) => {
        // tslint:disable-next-line: no-console
        console.log('Document id set as', docRef.id);
      })
      .catch(error => {
        // tslint:disable-next-line: no-console
        console.log('Error Adding Document', error);
      });
  }

  // getMasajid(): Observable<Masjid[]> {
  //   return this.masajidCollection.valueChanges();
  // }

  getMasjid() {
    this.masjidObservable$ = this.masajidCollection.snapshotChanges().pipe(
      map(action =>
        action.map(a => {
          const data = a.payload.doc.data() as Masjid;
          const id = a.payload.doc.id;
          return { data, id };
        }),
      ),
    );

    return this.masjidObservable$;
  }

  getBlank() {
    let blank$: Observable<{ }[]>;
    return blank$;
  }
  getVerifiedMasjids() {
    this.verifiedMasjids$ = this.afs
    .collection('masajid', ref => ref.where('verified', '==', true)).snapshotChanges().pipe(
      map(action =>
        action.map(a => {
          const data = a.payload.doc.data() as Masjid;
          const id = a.payload.doc.id;
          return { data, id };
        }),
      ),
    );

    return this.verifiedMasjids$;
  }

  getRequestedMasjids() {
    this.verifiedMasjids$ = this.afs
    .collection('masajid', ref => ref.where('verified', '==', false)).snapshotChanges().pipe(
      map(action =>
        action.map(a => {
          const data = a.payload.doc.data() as Masjid;
          const id = a.payload.doc.id;
          return { data, id };
        }),
      ),
    );

    return this.verifiedMasjids$;
  }


  getVerifiedMasajid() {
    this.verifiedMasjidObservable$ = this.afs
      .collection('masajid', ref => ref.where('verified', '==', true))
      .snapshotChanges()
      .pipe(
        map(action =>
          action.map(a => {
            const data = a.payload.doc.data() as Masjid;
            const id = a.payload.doc.id;
            const imamPromise: Promise<{ data: Masjid, id: string, imam?: User }> = new Promise((resolve, reject) => {
              if (data.imamRef) {
                const imamID = data.imamRef.id;
                this.afs.doc(`users/${imamID}`).ref.get()
                  .then(
                    (doc) => {
                      const imam = doc.data() as User;
                      resolve({ data, id, imam });
                    },
                  );
              } else {
                reject({ data, id });
              }
            });
            imamPromise.then(
              (dataWithImam: { data: Masjid, id: string, imam: User }) => dataWithImam,
            ).catch(
              (dataNoImam: { data: Masjid, id: string }) => dataNoImam,
            );
            return imamPromise;
          }),
        ),
      );
    return this.verifiedMasjidObservable$;
    // return this.masajidVerifiedObservable$;
  }

  getRequestedMasajid() {
    this.unverifiedMasjidObservable$ = this.afs
      .collection('masajid', ref => ref.where('verified', '==', false))
      .snapshotChanges()
      .pipe(
        map(action =>
          action.map(a => {
            const data = a.payload.doc.data() as Masjid;
            const id = a.payload.doc.id;
            const imamPromise: Promise<{ data: Masjid, id: string, suggestedBy?: User }> =
              new Promise((resolve, reject) => {
                if (data.suggestedBy) {
                  const userID = data.suggestedBy.id;
                  this.afs.doc(`users/${userID}`).ref.get()
                    .then(
                      (doc) => {
                        const suggestedBy = doc.data() as User;
                        resolve({ data, id, suggestedBy });
                      },
                    );
                } else {
                  reject({ data, id });
                }
              });
            imamPromise.then(
              (dataWithUser: { data: Masjid, id: string, suggestedBy: User }) => dataWithUser,
            ).catch(
              (dataNoUser: { data: Masjid, id: string }) => dataNoUser,
            );
            return imamPromise;
          }),
        ),
      );
    return this.unverifiedMasjidObservable$;
    // this.masajidObservable$ = this.masajidCollection.where('verified', '==', true).snapshotChanges().pipe(
    //   map(action =>
    //     action.map(a => {
    //       const data = a.payload.doc.data() as Masjid;
    //       const id = a.payload.doc.id;
    //       return { data, id };
    //     })
    //   )
    // );

    // return this.masajidObservable$;
  }


  deleteMasajid(masjidId: string) {
    const masjidRef = '/masajid/' + masjidId;
    console.log(masjidRef);
    const mainRef = this.afs.doc(masjidRef).ref;
    this.salah = this.afs.collection('salahs', ref => ref
    .where('masjidRef', '==', mainRef ))
    .snapshotChanges()
    .pipe(
      map(action =>
        action.map(a => {
          const id = a.payload.doc.id;
          return id;
        })
      )
    ).subscribe(id => this.deleteSalah(id));
    const document = this.masajidCollection.doc(`${masjidId}`);
    document
      .delete()
      .then(() => {
        console.log(`Document of id ${masjidId} deleted Successfully`);
      })
      .catch(error => {
        console.log('Error Removing Document', error);
      });
  }

  editMasjid(masjidData: any, masjidId: string) {
    const masjidRef: AngularFirestoreDocument<Masjid> = this.afs.doc(
      `masajid/${masjidId}`
    );

    masjidRef
      .update(masjidData)
      .then(() => {
        console.log(`successfully edited User`);
      })
      .catch(error => {
        console.error(error);
      });
  }

  // editMasajid(id: string, arr: any) {
  //   this.afs
  //     .doc(`masajid/${id}`)
  //     .valueChanges()
  //     .subscribe((masajidData: Masjid) => {
  //       // Inserting data by looping the arr obj
  //       // tslint:disable-next-line: forin
  //       for (const key in arr) {
  //         // tslint:disable-next-line: prefer-const
  //         let value = arr[key];
  //         masajidData[key] = value;
  //       }
  //       this.afs.doc(`masajid/${id}`).set(masajidData);
  //     });
  // }

  checkStatus(status: boolean = false) {
    return !status;
  }
  editStatus(masajidId: string, newStatus: boolean) {
    this.masajidCollection
      .doc(masajidId)
      .update({
        status: newStatus,
      })

      .then(() => {
        // tslint:disable-next-line: no-console
        console.log('Document successfully updated!');
      })

      .catch(error => {
        // tslint:disable-next-line: no-console
        console.log('Error found data not changed' + error);
      });
  }

  editVerification(masajidId: string, newStatus: boolean) {
    this.masajidCollection
      .doc(masajidId)
      .update({
        verified: newStatus,
        status: newStatus,
      })

      .then(() => {
        console.log('Document successfully updated!');
      })

      .catch(error => {
        console.log('Error found data not changed' + error);
      });
  }

  deleteMasajidWithImam(masjidId: string, imamaId: string) {
    const masjidRef = '/masajid/' + masjidId;
    console.log(masjidRef);
    const mainRef = this.afs.doc(masjidRef).ref;
    this.salah = this.afs.collection('salahs', ref => ref
    .where('masjidRef', '==', mainRef ))
    .snapshotChanges()
    .pipe(
      map(action =>
        action.map(a => {
          const id = a.payload.doc.id;
          return id;
        })
      )
    ).subscribe(id => this.deleteSalah(id));



    this.afs.collection('users').doc(imamaId)
    .update({
      masjidRef: firestore.FieldValue.delete(),
      'roles.imam' : false
    }).then(() => console.log('imam Set false'))
    .catch((error) => console.log(error));
    const document = this.masajidCollection.doc(`${masjidId}`);
    document
      .delete()
      .then(() => {
        console.log(`Document of id ${masjidId} deleted Successfully`);
      })
      .catch(error => {
        console.log('Error Removing Document', error);
      });
  }

  deleteSalah(data: string[]) {
    data.forEach(salah => this.afs.collection('salahs').doc(salah).delete()
    .then(() => console.log('Done'))
    .catch((err) => console.log(err))
    );
    this.salah.unsubscribe();

  }
}
