import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { TablesRoutingModule, routedComponents } from './masjids-routing.module';
import { UsersComponent } from './masjids.component';
import { SmartTableComponent } from './smart-table/smart-table.component';
import { AddMasjidComponent } from './add-masjid/add-masjids.component';
import { MasjidAdminTableComponent } from './masjid-admin/masjid-admin-table.component';
import { CustomRenderComponent } from './smart-table/button';
import { ViewButtonComponent } from './smart-table/viewButton';
import { ActivateButton } from './smart-table/activateButton';
import { ProfilePicModule } from '../../profile-pic/profile-pic.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ThemeModule,
    TablesRoutingModule,
    Ng2SmartTableModule,
    ProfilePicModule,
    FormsModule,

  ],
  entryComponents : [
    CustomRenderComponent,
    ViewButtonComponent,
    ActivateButton,
  ],
  declarations: [
    SmartTableComponent,
    UsersComponent,
    AddMasjidComponent,
    MasjidAdminTableComponent,
    CustomRenderComponent,
    ViewButtonComponent,
    ActivateButton,


  ],
})
export class MasjidsModule {
}
