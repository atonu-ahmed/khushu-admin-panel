import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableData } from '../../../@core/data/smart-table';

@Component({
  selector: 'ngx-masjid-admin-table',
  templateUrl: './masjid-admin-table.component.html',
  styles: ['./masjid-admin-table.component.scss'],
})
export class MasjidAdminTableComponent implements OnInit {
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      masjidImage: {
        title: 'Masjid Image',
        type: 'string',
      },
      masjidName: {
        title: 'Masjid Name',
        type: 'string',
      },
      masjidAddress: {
        title: 'Masjid Address',
        type: 'string',
      },
      city: {
        title: 'City',
        type: 'string',
      },
      state: {
        title: 'State',
        type: 'string',
      },
      country: {
        title: 'Country',
        type: 'string',
      },
      masjidEmail: {
        title: 'Masjid E-mail',
        type: 'string',
      },
      masjidAdmin: {
        title: 'Masjid Admin',
        type: 'string',
      },
      confirm: {
        title: 'Confirm',
        type: 'string',
      },
    },
  };
  adminApproveTableSettings = {
    add: {
      addButtonContent: 'Confirm',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-checkmark"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-close"></i>',
      confirmDelete: true,
    },
    columns: {
      masjidImage: {
        title: 'Masjid Image',
        type: 'string',
      },
      masjidName: {
        title: 'Masjid Name',
        type: 'string',
      },
      masjidAddress: {
        title: 'Masjid Address',
        type: 'string',
      },
      city: {
        title: 'City',
        type: 'string',
      },
      state: {
        title: 'State',
        type: 'string',
      },
      country: {
        title: 'Country',
        type: 'string',
      },
      masjidEmail: {
        title: 'Masjid E-mail',
        type: 'string',
      },
      masjidAdmin: {
        title: 'Masjid Admin',
        type: 'string',
      },
      confirm: {
        title: 'Confirm',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData) {}
  ngOnInit(): void {
    // Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    // Add 'implements OnInit' to the class.
    const data = this.service.getData();
    this.source.load(data);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
