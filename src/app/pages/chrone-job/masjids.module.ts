import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { TablesRoutingModule, routedComponents } from './masjids-routing.module';
import { UsersComponent } from './masjids.component';
import { SmartTableComponent } from './smart-table/smart-table.component';
import { AddMasjidComponent } from './add-masjid/add-masjids.component';
import { MasjidAdminTableComponent } from './masjid-admin/masjid-admin-table.component';


@NgModule({
  imports: [
    ThemeModule,
    TablesRoutingModule,
    Ng2SmartTableModule,

  ],
  declarations: [
    SmartTableComponent,
    UsersComponent,
    AddMasjidComponent,
    MasjidAdminTableComponent,

  ],
  // exports: [
  //   SmartTableComponent,
  //   UsersComponent,
  //   AddUserComponent,
  //   MasjidAdminTableComponent,
  // ],
})
export class MasjidsModule {
}
