import { Component, Input, OnInit } from '@angular/core';

import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { UserData } from '../../../@core/data/users';
import { AnalyticsService } from '../../../@core/utils';
import { LayoutService } from '../../../@core/utils';
import { AuthService } from '../../../auth.service';
import { WindowService } from '../../../window.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserService } from '../../../pages/users/user.service';
import { User } from '../../../models/user.model';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  @Input() position = 'normal';

  user: any;

  userMenu = [{ title: 'Profile' }, { title: 'Log out',}];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private userService: UserService,
              private analyticsService: AnalyticsService,
              private auth: AuthService,
              private layoutService: LayoutService,

              public win: WindowService,
              private afAuth: AngularFireAuth,
              private afs: AngularFirestore,
              private router: Router
              ) {
  }

  ngOnInit() {

    this.userService.getUserInfo(this.afAuth.auth.currentUser.uid).subscribe(
      (data) => {
        this.user = { name: data.name.firstName, picture: data.photoURL};
      },
    );
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }

  onMenuItemClick(event) {
    console.log("CLICK", event);
    this.auth.signOut();
    // console.log(this.auth.currentUser());
    }
}
