import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-profile-pic',
  templateUrl: './profile-pic.component.html',
  styleUrls: ['./profile-pic.component.css']
})
export class ProfilePicComponent implements OnInit {

  constructor() { }

  @Input() value: any;
  @Input() rowData: any;

  ngOnInit() {
    console.log(this.value);
  }

}
