/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  _firebase: {
    apiKey: 'AIzaSyDovM9rs7MOyh-24hoaXSCIkz5Qgwa_Cbo',
    authDomain: 'khushu-6b3f2.firebaseapp.com',
    databaseURL: 'https://khushu-6b3f2.firebaseio.com',
    projectId: 'khushu-6b3f2',
    storageBucket: 'khushu-6b3f2.appspot.com',
    messagingSenderId: '675607554587'
  },
  get firebase() {
    return this._firebase;
  },
  set firebase(value) {
    this._firebase = value;
  },
};
