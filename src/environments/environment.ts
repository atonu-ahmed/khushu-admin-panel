// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  _firebase: {
    apiKey: 'AIzaSyDovM9rs7MOyh-24hoaXSCIkz5Qgwa_Cbo',
    authDomain: 'khushu-6b3f2.firebaseapp.com',
    databaseURL: 'https://khushu-6b3f2.firebaseio.com',
    projectId: 'khushu-6b3f2',
    storageBucket: 'khushu-6b3f2.appspot.com',
    messagingSenderId: '675607554587'
  },
  get firebase() {
    return this._firebase;
  },
  set firebase(value) {
    this._firebase = value;
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
